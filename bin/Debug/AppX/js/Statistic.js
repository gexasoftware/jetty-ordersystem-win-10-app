var Statistics = function (){
    this.startDate      = (typeof sessionDate != 'undefined') ? sessionDate : moment().format('YYYY-MM-DD');
    this.periodStart    = '';
    this.periodEnd      = '';

    this.isLogin        = false;

    this.initCharger();
    this.init();
}

Statistics.prototype.initCharger = function () {
    var self = this,
        minYear = 2018,
        strYearHtml = '';

    for (var y = new Date().getFullYear(); y >= minYear; y--) {
        strYearHtml += '<option value="' + y + '">' + y + '</option>';
    }

    $('#dailyYearSelect, #monthlyYearSelect').html(strYearHtml);

    $('#dailyDatePicker').flatpickr({
        maxDate: moment().format('YYYY-MM-DD')
        , defaultDate: this.startDate
    });

    $("#monthlyDatePickerStart").flatpickr({
        "plugins": [
            new rangePlugin({ input: "#monthlyDatePickerEnd" })
        ]
    });

    $('a[data-toggle="tab"][href="#reports"]').on('shown.bs.tab', function (e) {
        if (self.isLogin && (typeof sessionDate == 'undefined' || self.startDate == sessionDate))
            self.getDailyStatistic();
    });

    // $('a[data-toggle="tab"][href="#reports"]').on('hide.bs.tab', function (e) { });
}

Statistics.prototype.init = function () {
    var self = this;

    //google.charts.load('current', { packages: ['corechart', 'bar'] });

    $('#dailyDatePicker').on('change', function () {
        self.startDate = $(this).val();
        self.getDailyStatistic();
    });

    $('#monthlyDatePickerStart').on('change', function () {
        self.periodStart = $(this).val();
        self.periodEnd = $('#monthlyDatePickerEnd').val();
        if (self.periodEnd != '')
            self.getPeriodStatistic();
    });

    $('#login').on('submit', function (e) {
        e.preventDefault();
        self.loginStatistic();
    });

    $('#logoutToStatistics').on('click', function () {
        self.logoutStatistic();
    });

    var DateTimeFormats = {
        second: '%Y-%m-%d<br/>%H:%M:%S',
        minute: '%Y-%m-%d<br/>%H:%M',
        hour: '%Y-%m-%d<br/>%H:%M',
        day: '%Y<br/>%m-%d',
        week: '%Y<br/>%m-%d',
        month: '%Y-%m',
        year: '%Y'
    };
    var groupingUnits = [['week', [1]], ['month', [1, 2, 3, 4, 5]]];
}

Statistics.prototype.loginStatistic = function () {
    var self = this;
    var objLogName = $('#loginName');
    var logName = objLogName.val();
    var objLogPass = $('#loginPass');
    var logPass = objLogPass.val();

    var mameIsValid = validateInput(logName, objLogName);
    var passIsValid = validateInput(logPass, objLogPass);

    if (!mameIsValid || !passIsValid) {
        return false;
    }

    Easy.GW('statistic->loginStatistic', {
        log_name: logName
        , log_pass: logPass
    }).done(function (JSONResponse) {
        if (JSONResponse.data.success == 1) {
            if (JSONResponse.data.is_valid) {
                self.isLogin = true;
                objLogPass.removeClass('login-error');
                objLogName.val('');
                objLogPass.val('');
                $('#loginContent').hide();
                $('#reportContent, #logoutToStatistics').show();
                self.getDailyStatistic();
            } else {
                objLogPass.addClass('login-error');
            }
        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });

    function validateInput(needString, objElement) {
        if (needString == '') {
            objElement.removeClass('login-error');
            objElement.addClass('in-valid');
            return false;
        }

        objElement.removeClass('in-valid');
        return true;
    }

}

Statistics.prototype.logoutStatistic = function () {
    self.isLogin = false;
    $('#reportContent, #logoutToStatistics').hide();
    $('#loginContent').show();
    $('#dailyIncome, #dailyInfo, #periodItem, #periodIncome, #periodInfo').html('');
    $('#monthlyDatePickerEnd, #monthlyDatePickerStart').val('');
}

Statistics.prototype.getDailyStatistic = function(){
    var self = this;

    Easy.GW('statistic->getDailyStatistic', {
        date: this.startDate
    }).done(function(JSONResponse) {
        if (JSONResponse.data.success == 1){
            self.drawDailyStatistic(JSONResponse.data.statistic[self.startDate], JSONResponse.data.income);
        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });
}


Statistics.prototype.getPeriodStatistic = function(){
    var self = this;

    Easy.GW('statistic->getPeriodStatistic', {
        date_start: this.periodStart
        , date_end: this.periodEnd
    }).done(function(JSONResponse) {
        if (JSONResponse.data.success == 1){
            console.log(JSONResponse.data.statistic, JSONResponse.data.income);
            self.drawPeriodStatistic(JSONResponse.data.statistic, JSONResponse.data.income);
        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });
}


Statistics.prototype.drawDailyStatistic = function(results, tblSumIncome) {
    var itemsData = [],
        products = [],
        data = [],
        self = this;

    if (!results)
        return;

    $.each(results, function (id, item) {
        products.push(item.cat_name);
        itemsData.push({ 
            'name': item.cat_name,
            'color': item.color,
            'y': item.sum,
            'tooltip': self.getTooltipHtml(item.items, item.cat_name)
        });
    });

    var highchartsOptions = Highcharts.setOptions({
        lang: {
            loading: 'Betöltés...',
            xportButtonTitle: "Exportál",
            printButtonTitle: "Importál",
            downloadPNG: 'Letöltés PNG képként',
            downloadJPEG: 'Letöltés JPEG képként',
            downloadPDF: 'Letöltés PDF dokumentumként',
            downloadSVG: 'Letöltés SVG formátumban',
            downloadSVG: 'Letöltés SVG formátumban',
            resetZoom: "Visszaállít",
            resetZoomTitle: "Visszaállít",
            thousandsSep: " ",
            decimalPoint: ','
        },
        credits: { enabled: false },
        chart: { zoomType: 'xy' }
    });

    $('#dailyIncome').css({ width: '100%', height: 400 }).highcharts({
        chart: {
            type: 'column'
        },
        tooltip: {
            useHTML: true,
            formatter: function () {
                return this.point.tooltip;
            },
            split: true
        },
        title: {
            text: 'Termékek szerinti fogyasztás adott időszakra'
        },
        xAxis: {
            categories: products,
            title: {
                text: 'Termék'
            }
        },
        yAxis: {
            title: {
                text: 'Teljes fogyasztás'
            }
        },
        series: [{
            name: 'Eladások (Ft)',
            data: itemsData
        }]
    });

    drawSumIncome();


    function drawSumIncome(){
        var strHtml = '';
        strHtml += '<ul class="statistic-info">';
        strHtml += '<li class="income">Összes bevétel: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.total) + ' Ft</strong></li>';
        strHtml += '<li class="income">Ebből bankkártyával fizetett: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.totalCard) + ' Ft</strong></li>';
        if (tblSumIncome.not_paid_items != 0) {
            strHtml += '<li class="not-pay">Összes nem fizetett fogyasztás: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.not_paid_items) + ' Ft</strong></li>';
        }

        strHtml += '<li class="tip">Összes borravaló: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.tip) + ' Ft</strong></li>';
        strHtml += '</ul>';

        $('#dailyInfo').html(strHtml);
    }

}



Statistics.prototype.drawPeriodStatistic = function(tblData, tblSumIncome){
    var self = this;
    
    var itemsData = [],
        products = [],
        data = [],
        self = this;

    if (!tblData)
        return;

    var highchartsOptions = Highcharts.setOptions({
        lang: {
            loading: 'Betöltés...',
            xportButtonTitle: "Exportál",
            printButtonTitle: "Importál",
            downloadPNG: 'Letöltés PNG képként',
            downloadJPEG: 'Letöltés JPEG képként',
            downloadPDF: 'Letöltés PDF dokumentumként',
            downloadSVG: 'Letöltés SVG formátumban',
            downloadSVG: 'Letöltés SVG formátumban',
            resetZoom: "Visszaállít",
            resetZoomTitle: "Visszaállít",
            thousandsSep: " ",
            decimalPoint: ','
        },
        credits: { enabled: false },
        chart: { zoomType: 'xy' }
    });

    drawPeriodItem();
    drawSumIncome();

    function drawPeriodItem(){
        
        var tblItems = {};

        $.each(tblData, function(i, e){
            $.each(e, function(catId, objCat){
                if (typeof tblItems[catId] == 'undefined'){
                    tblItems[catId] = objCat;
                } else {
                    tblItems[catId].sum += objCat.sum;
                    $.each(objCat.items, function(itemId, objItem){
                        if (typeof tblItems[catId].items[itemId] == 'undefined'){
                            tblItems[catId].items[itemId] = objItem;
                        } else {
                            tblItems[catId].items[itemId].piece += objItem.piece;
                        }
                    });
                }
            });
        });

        var rowData = [];
        $.each(tblItems, function(i, e){
            products.push(e.cat_name);
            rowData.push({
                'name': e.cat_name,
                'color': e.color,
                'y': e.sum,
                'tooltip': self.getTooltipHtml(e.items, e.cat_name)
            });
        });


        $('#periodIncome').css({ width: '100%', height: 400 }).highcharts({
            chart: {
                type: 'column'
            },
            tooltip: {
                useHTML: true,
                formatter: function () {
                    return this.point.tooltip;
                },
                split: true
            },
            title: {
                text: 'Fogyasztás napok szerint'
            },
            xAxis: {
                categories: products,
                title: {
                    text: 'Termék'
                }
            },
            yAxis: {
                title: {
                    text: 'Teljes fogyasztás'
                }
            },
            series: [{
                name: 'Termék értékesítések (Ft)',
                data: rowData
            }]
        });

    }

    function drawSumIncome(){
        var strHtml = '';
        strHtml += '<ul class="statistic-info">';
        strHtml += '<li class="income">Összes bevétel: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.total) + ' Ft</strong></li>';
        strHtml += '<li class="income">Bankkártyával fizetve: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.totalCard) + ' Ft</strong></li>';
        if (tblSumIncome.not_paid_items != 0)
            strHtml += '<li class="not-pay">Összes nem fizetett fogyasztás: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.not_paid_items) + ' Ft</strong></li>';

        strHtml += '<li class="tip">Összes borravaló: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.tip) + ' Ft</strong></li>';
        strHtml += '</ul>';

        $('#periodInfo').html(strHtml);
    }

}


Statistics.prototype.getTooltipHtml = function(tblItems, strCatName){
    var numSum = 0;
    var strHtml = '<div class="statistic-tooltip">';
    strHtml += '<h2>' + strCatName + '</h2>';

    strHtml += '<ul class="tooltip-list">';
    $.each(tblItems, function(numItemId, objItem){
        strHtml += '<li>' + objItem.name + (objItem.unit != '' ? ' (' + objItem.unit + ')' : '') + ': <strong>' + objItem.piece + ' * ' + new Intl.NumberFormat('hu-HU').format(objItem.price) + ' Ft</strong></li>';
        numSum += parseInt(objItem.piece * objItem.price);
    });
    strHtml += '</ul>';
    strHtml += '<div class="sum">Összesen: <span>' + new Intl.NumberFormat('hu-HU').format(numSum) + ' Ft</span></div>';
    strHtml += '</div>';

    return strHtml;
}

var Statistics = new Statistics();