/*
 * Ezt az osztály felelős a fogyasztási lista megjelenítésért és a termékek hozzárendelésért
 */
var Consumption = function (placeId, tableId) {

    this.objParentTable = Place[placeId].tables[tableId];

    this.$objList;
    this.$objSum;
    this.$objPiece;
    this.$objMenu;
    this.$objGuestName;

    this.numActiveCategory;
    this.numActiveItemId;

    this.isSelectPart;

    this.init();
}

Consumption.prototype.init = function () {
    var self = this;

    var strSkeleton = getConsumptionSkeleton();
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DEFAULT
        , animate: false
        , title: '<h4 class="head-table-name">' + this.objParentTable.tableName + '</h4> - Asztal rendelései'
        , message: strSkeleton
        , buttons: [{
            label: 'Teljes lista kifizetése'
            , icon: 'fa fa-money'
            , hotkey: 27
            , action: function (dialogRef) {
                self.payment(true, dialogRef);
            }
        }, {
            label: 'Listarész kifizetése'
            , icon: 'fa fa-money'
            , hotkey: 27
            , action: function (dialogRef) {
                self.payment(false, dialogRef);
            }
        }, {
            label: 'Teljes lista áthelyezése'
            , icon: 'fa fa-share'
            , hotkey: 27
            , action: function (dialogRef) {
                dialogRef.close();
                self.sitOver(true);
            }
        }, {
            label: 'Listarész áthelyezése'
            , icon: 'fa fa-share'
            , hotkey: 27
            , action: function (dialogRef) {
                dialogRef.close();
                if (Object.keys(self.objParentTable.tblConsumptionList).length > 0) {
                    self.sitOver(false);
                } else {
                    self.sitOver(true);
                }
            }
        }, {
            label: 'Bezár'
            , icon: 'fa fa-remove'
            , hotkey: 27
            , action: function (dialogRef) {
                if (Object.keys(self.objParentTable.tblConsumptionList).length < 1) {
                    self.payment(true, dialogRef);
                }
                self.close(dialogRef);
            }
        }]
        , modal: true
        //, closable: false
        , cssClass: 'full-dialog'
        , onshown: initOnShown
        , onhidden: function () {
            delete self;
        }
    });

    function initOnShown() {
        self.$objGuestName = $('#guestName');
        self.$objList = $('#consuptionList');
        self.$objSum = $('#consuptionListSum');
        self.$objPiece = $('#pieceSelector');
        self.$objMenu = $('#MenuList');

        self.$objList.css({
            height: $(window).height() - 320
        });

        var objMenuDiff = 219;
        if (pagers != 0) {
            objMenuDiff += 65;
        }
        if (item_add_on_click != 1) {
            objMenuDiff += 98;
        }

        var objMenuHeight = $(window).height() - objMenuDiff;
        self.$objMenu.css({
            height: objMenuHeight
            , overflow: 'hidden'
            , position: 'relative'
        });

        self.refreshSum();
        self.eventListener();
        $('.consuption-left, .consuption-right').animate({ opacity: 1 }, 200);
    }

    function getConsumptionSkeleton() {
        var strConsumptionList = '';
        $.each(self.objParentTable.tblConsumptionList, function (i, e) {
            strConsumptionList += self.getConsListItem(e, false);
        });

        var strMenuCategoryList = '<ul id="MenuCategoryList">';
        var strMenuCategoryItemList = '';
        var itemIndex = 0;
        $.each(Menu.categories, function (ci, ce) {
            if (Object.keys(ce.items).length > 0) {
                strMenuCategoryList += '<li id="MenuCategory_' + ce.categoryId + '" class="menu-list-category" data-id="' + ce.categoryId + '" style="background-color: ' + ce.categoryOptions.color + '"> \
					<h4>'+ ce.categoryName + '</h4> \
				</li>';

                strMenuCategoryItemList += '<ul id="MenuCategoryItemList_' + ce.categoryId + '" class="menu-item-list-wrapper' + (itemIndex > 0 ? '' : ' first') + '">';
				/*strMenuCategoryItemList += '<li class="back-menu-list" style="background-color: '+ce.categoryOptions.color+'"> \
					<h3>'+ce.categoryName+'</h3>\
					<i class="fa fa-chevron-left"></i> Vissza a kategóriákhoz \
				</li>';*/

                $.each(ce.items, function (ii, ie) {
                    Menu.tblItemList[ie.itemId] = ie;
                    strMenuCategoryItemList += '<li id="MenuItem_' + ie.itemId + '" data-id="' + ie.itemId + '" class="menu-list-item"> \
						<h4>'+ ie.itemName + '</h4>';
					/*if (ie.itemMoreType == 1){
						$.each(ie.itemPrice, function(iepi, iepv){
							strMenuCategoryItemList += '<div class="menu-item-price-units"> \
								'+iepv.price+'Ft <span>/ '+iepv.unit_piece+iepv.price_units+'</span> \
							</div>';
						});
					} else {
						strMenuCategoryItemList += '<div class="menu-item-price">';
						strMenuCategoryItemList += ie.itemPrice.price+'Ft ';
						if (ie.itemPrice.unit_piece!=''){
							strMenuCategoryItemList += '<span>'+ie.itemPrice.unit_piece+ie.itemPrice.price_units+'</span>';
						}
						strMenuCategoryItemList += '</div>';
					}*/
                    strMenuCategoryItemList += '</li>';

                });

                strMenuCategoryItemList += '</ul>';
                itemIndex++;
            }
        });
        strMenuCategoryList += '</ul>';

        if (pagers != 0) {
            var
                pagerHTML = '<div class="row table-pager py-2"> \
							<div class="col-sm-12 btn-group"> \
								<button class="btn btn-danger btn-up w-25"><span class="fa fa-caret-up"></span></button> \
								<button class="btn btn-danger btn-down w-25"><span class="fa fa-caret-down"></span></button> \
								<button class="btn btn-warning btn-up-items w-25"><span class="fa fa-caret-up"></span></button> \
								<button class="btn btn-warning btn-down-items w-25"><span class="fa fa-caret-down"></span></button> \
							</div> \
						</div>';
        }

        var strHtml = '<div class="table-consuption row"> \
			<div class="consuption-left col-sm-5"> \
				<div class="guest-block"> \
					<label>Vendég neve:</label> \
					<span id="guestName">'+ self.objParentTable.tblConsumptionData.guest_name + '</span> \
					<button class="btn btn-warning" id="editGuestName"><i class="fa fa-pencil"></i></button> \
				</div> \
				<div class="consuption-list"> \
					<ul id="consuptionList"> \
					'+ strConsumptionList + ' \
					</ul> \
					<div id="consuptionListSum"> \
					</div> \
				</div> \
			</div> \
			<div class="consuption-right col-sm-7"> \
				'+ (item_add_on_click != 0 ? ('<div id="pieceSelector" class="hidden"><input type="hidden" id="itemPriceInput" value="1"></div>') : '<div id="pieceSelector" class="mb-3"> \
					<label>Termék neve: <span class="item-name"></span><span class="piece-units" data-index="-1"></span></label> \
					<div class="input-group"> \
						<div class="input-group-btn"> \
							<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field-id="itemPriceInput"> \
								<span class="fa fa-minus"></span> \
							</button> \
						</div> \
						<input type="number" id="itemPriceInput" class="form-control input-number" disabled="disabled" min="1" value="1"/> \
						<div class="input-group-btn"> \
							<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="plus" data-field-id="itemPriceInput"> \
								<span class="fa fa-plus"></span> \
							</button> \
						</div> \
						<div class="input-group-btn"> \
							<button class="btn btn-success" disabled="disabled" id="addItemList"> \
								<i class="fa fa-plus"></i> Listához ad \
							</button> \
						</div> \
					</div> \
				</div>') + ' \
				<div id="MenuList"> \
				'+ strMenuCategoryList + ' \
				'+ strMenuCategoryItemList + ' \
				</div> \
				'+ (!pagers ? '' : pagerHTML) + ' \
			</div> \
		</div>';

        return strHtml;
    }
}

Consumption.prototype.getConsListItem = function (objConsItemData, isSelect) {
    if (typeof isSelect == 'undefined') isSelect = false;
    var objItemPrice = JSON.parse(objConsItemData.price);
    var piece_units = objItemPrice.unit_piece + objItemPrice.price_units;
    var strHtml = '';
    var numItemSumPrice = Math.round(objItemPrice.price * objConsItemData.piece);

    var strListElemClass = '';
    var strListElemInputId = 'consItemPriceInput';
    var strListCheckboxElem = '';
    var strListElemDel = '';
    var strListElemMax = '';
    var strListElemValue = objConsItemData.piece;
    var strListElemDecDisab = '';
    var strListElemIncDisab = '';

    if (!isSelect) {
        strListElemClass = 'consumption-list-item';
        strListElemDel = '<div class="input-group-btn"> \
			<button type="button" class="btn btn-danger cons-item-del"> \
				<span class="fa fa-trash"></span> \
			</button> \
		</div>';
    } else {
        strListElemClass = 'consumption-selectlist-item disabled';
        strListElemInputId = 'selectConsItemPriceInput'
        strListCheckboxElem = '<div class="checkbox"> \
			<label style="font-size: 2em"> \
				<input type="checkbox" class="select-checkbox"> \
				<span class="cr"><i class="cr-icon fa fa-check"></i></span> \
			</label> \
		</div>';
        strListElemMax = ' max="' + objConsItemData.piece + '"';
        //strListElemValue = 1;

        strListElemDecDisab = ' disabled';
        strListElemIncDisab = ' disabled';

        //numItemSumPrice = objItemPrice.price;
    }

    // (piece_units!=''?'<span>('+piece_units+')</span>':'')+
    strHtml += ' \
	<li class="'+ strListElemClass + '" data-id="' + objConsItemData.id + '" data-price="' + objItemPrice.price + '"> \
		' + strListCheckboxElem + ' \
		<div class="item-text">\
			<h4>'+ objConsItemData.name + '</h4> \
			<span class="date">'+ objConsItemData.created_at + '</span> \
		</div> \
		<div class="item-options"> \
			<div class="cons-item-piece input-group"> \
				<div class="input-group-btn"> \
					<button type="button" class="btn btn-info btn-number" data-type="minus" data-field-id="'+ strListElemInputId + '_' + objConsItemData.id + '"' + strListElemDecDisab + '> \
						<span class="fa fa-minus"></span> \
					</button> \
				</div> \
				<input type="text" id="'+ strListElemInputId + '_' + objConsItemData.id + '" class="form-control input-number numpad" disabled min="1"' + strListElemMax + ' value="' + strListElemValue + '"/> \
				<div class="input-group-btn"> \
					<button type="button" class="btn btn-info btn-number" data-type="plus" data-field-id="'+ strListElemInputId + '_' + objConsItemData.id + '"' + strListElemIncDisab + '> \
						<span class="fa fa-plus"></span> \
					</button> \
				</div> \
				<div class="input-group-addon"> \
					<div class="cons-item-price" data-sum-price="'+ numItemSumPrice + '"> \
						'+ numItemSumPrice + '<span>Ft</span> \
					</div> \
				</div> \
				' + strListElemDel + ' \
			</div> \
		</div> \
	</li>';

    return strHtml;
}

Consumption.prototype.getConsListSelectItem = function () {
    var objRes = [];
    if ($('.consumption-selectlist-item').length > 0) {
        $.each($('.consumption-selectlist-item .select-checkbox'), function (i, e) {
            if ($(e).is(':checked')) {
                var objParent = $(e).parents(".consumption-selectlist-item").first();
                var newPayElement = {};
                newPayElement.id = objParent.data('id');
                newPayElement.count = objParent.find('.input-number').val();
                objRes.push(newPayElement);
            }
        });
    }

    return objRes;
}

Consumption.prototype.refreshDraw = function () {
    this.$objGuestName.text(this.objParentTable.tblConsumptionData.guest_name);
}

Consumption.prototype.refreshSum = function () {
    var self = this,
        numSum = 0,
        strHtml = '';

    $.each(this.$objList.find('.cons-item-price'), function () {
        var numItemPrice = parseInt($(this).attr('data-sum-price'));

        numSum += numItemPrice;
    });

    strHtml += '<span>Végösszeg:</span>';
    strHtml += '<div class="pull-right cons-sum-price">' + numberFormat(numSum) + ' <span>Ft</span></div>';

    this.$objSum.html(strHtml);

    function numberFormat(number) {
        number += '';
        var x = number.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2');
        }
        return x1 + x2;
    }
}

Consumption.prototype.close = function (dialogRef) {
    $('#editGuestName').off('click');

    dialogRef.close();
}

Consumption.prototype.eventListener = function () {
    var self = this;

    self.initPieceListener();

    $('#editGuestName').on('click', function () {
        self.objParentTable.editGuest();
    });

    this.numActiveCategory = $('.menu-list-category:first').data('id');

    $('.menu-list-category').on('click', function () {
        var numMenuCatId = $(this).data('id');
        self.slideMenuCategory(numMenuCatId);
    });

    $('.back-menu-list').on('click', function () {
        self.slideMenuCategory();
        self.emptyingPieceHtml();
    });

    $('.table-pager').find('.btn').on('click', function () {
        var active = self.numActiveCategory;
        if ($(this).hasClass('btn-down')) {
            // DOWN
            var $activeElement = $('#MenuCategoryList');
            $activeElement.animate({
                scrollTop: $activeElement.scrollTop() + 120
            }, 10);
        } else if ($(this).hasClass('btn-up')) {
            // UP
            var $activeElement = $('#MenuCategoryList');
            $activeElement.animate({
                scrollTop: $activeElement.scrollTop() - 120 <= 0 ? 0 : $activeElement.scrollTop() - 120
            }, 10);
        } else if ($(this).hasClass('btn-up-items')) {
            var $activeElement = $('#MenuCategoryItemList_' + active);
            $activeElement.animate({
                scrollTop: $activeElement.scrollTop() - 120 <= 0 ? 0 : $activeElement.scrollTop() - 120
            }, 10);
        } else if ($(this).hasClass('btn-down-items')) {
            var $activeElement = $('#MenuCategoryItemList_' + active);
            $activeElement.animate({
                scrollTop: $activeElement.scrollTop() + 120
            }, 10);
        }
    });

    $('.menu-list-item, .menu-list-item .menu-item-price-units').on('click', function (e) {
        e.stopPropagation();
        if ($(this).hasClass('menu-item-price-units')) {
            var index = $(this).index() - 1;
            var numMenuItemId = $(this).parent().data('id');
            self.selectItem(numMenuItemId, index);
        } else {
            var numMenuItemId = $(this).data('id');
            self.selectItem(numMenuItemId);
        }
    });

    $('#addItemList').on('click', function () {
        self.addItemToList();
    });
}

Consumption.prototype.initPieceListener = function () {
    var self = this;
    var clickEventTimeout = [];

    $('.btn-number').off('click');
    $('.input-number').off('change');
    $('.cons-item-del').off('click');
    $('.select-checkbox').off('change');

    $('.numpad').removeAttr('disabled').prop('readonly', 'readonly').keypad({
        keypadOnly: false
        , showAnim: 'slide'
        , showOptions: { direction: 'up' }
        , duration: 'fast'
        , layout: ['123' + $.keypad.CLOSE, '456' + $.keypad.CLEAR, '789', '0']
        , onClose: function () {
            self.updateItemToList($(this).parents('.consumption-list-item:first').data('id'), $(this).val());
        }
    });

    $('.btn-number').click(function (e) {
        e.preventDefault();

        var fieldName = $(this).attr('data-field-id');
        var type = $(this).attr('data-type');
        var input = $("input#" + fieldName);
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {
                if (typeof input.attr('min') == 'undefined' || currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (typeof input.attr('min') != 'undefined' && parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }
            } else if (type == 'plus') {
                if (typeof input.attr('max') == 'undefined' || currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (typeof input.attr('max') != 'undefined' && parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }
            }
        } else {
            input.val(1);
        }

        if ($(this).parents('.consumption-list-item').length > 0) {
            var numModItemId = $(this).parents('.consumption-list-item').attr('data-id');
            if (clickEventTimeout[numModItemId]) {
                clearTimeout(clickEventTimeout[numModItemId]);
            }

            clickEventTimeout[numModItemId] = setTimeout(function () {
                self.updateItemToList(numModItemId, input.val());
            }, 500);
        }

        if ($(this).parents('.consumption-selectlist-item').length > 0) {
            var objParentLi = $(this).parents('.consumption-selectlist-item');
            var numModItemPrice = objParentLi.attr('data-price');
            var numNewPiece = objParentLi.find('.input-number').val();
            var numConsItemSumPrice = Math.round(numModItemPrice * numNewPiece);
            var strConsItemPrice = numConsItemSumPrice + '<span>Ft</span>';
            objParentLi.find('.cons-item-price').attr('data-sum-price', numConsItemSumPrice).html(strConsItemPrice);
        }

    });

    $('.input-number').change(function () {

        var id = $(this).attr('id');
        var valueCurrent = parseInt($(this).val());
        var numMin = parseInt($(this).prop('min'));
        var numMax = parseInt($(this).prop('max'));

        if (valueCurrent > numMax) {
            $(this).val(numMax);
        } else if (valueCurrent < numMin) {
            $(this).val(numMin);
        }

        if (typeof $(this).attr('min') != 'undefined') {
            var minValue = parseInt($(this).attr('min'));
            if (minValue && valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field-id='" + id + "']").removeAttr('disabled')
            }
        }

        if (typeof $(this).attr('max') != 'undefined') {
            var maxValue = parseInt($(this).attr('max'));
            if (maxValue && valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field-id='" + id + "']").removeAttr('disabled')
            }
        }
    });

    $('.cons-item-del').click(function (e) {
        var numModItemId = $(this).parents('.consumption-list-item').attr('data-id');
        self.deleteItemFromList(numModItemId);
    });

    $('.select-checkbox').change(function () {
        var objParent = $(this).parents('li').first();
        var numModBtn = objParent.find('.btn-number');
        var objImput = objParent.find('.input-number');
        if (this.checked) {
            numModBtn.removeAttr('disabled');
            objImput.removeAttr('disabled');
            objParent.removeClass('disabled');
        } else {
            numModBtn.attr('disabled', true);
            objImput.attr('disabled');
            objParent.addClass('disabled');
        }
    });
}

Consumption.prototype.slideMenuCategory = function (numId) {
    var numSlideCat = null;
    var numCatLeft = '0';
    var numItemLeft = '100%';
    if (!numId) {
        numSlideCat = this.numActiveCategory;
        this.removeSelectItem();
    } else {
        numSlideCat = numId;
        numCatLeft = '-100%';
        numItemLeft = '0';
    }
    this.numActiveCategory = numId;

	/*$('#MenuCategoryList').css({
		"-webkit-transform"	: 'translateX( '+numCatLeft+' )',
		"-ms-transform"		: 'translateX( '+numCatLeft+' )',
		"transform"			: 'translateX( '+numCatLeft+' )'
	}, 200);
	$('#MenuCategoryItemList_'+numSlideCat).css({
		"-webkit-transform"	: 'translateX( '+numItemLeft+' )',
		"-ms-transform"		: 'translateX( '+numItemLeft+' )',
		"transform"			: 'translateX( '+numItemLeft+' )'
	}, 200);*/
    $('.menu-item-list-wrapper').hide();
    $('#MenuCategoryItemList_' + numSlideCat).fadeIn(200);
}

Consumption.prototype.removeSelectItem = function () {
    $('.menu-list-item, .menu-list-item .menu-item-price-units').removeClass('active');
}

Consumption.prototype.selectItem = function (numId, priceIndex) {
    if (!priceIndex) priceIndex = 0;

    this.removeSelectItem();
    this.fillPieceHtml(numId, priceIndex);
    $('#MenuItem_' + numId).addClass('active');
    $('#MenuItem_' + numId + ' .menu-item-price-units').eq(priceIndex).addClass('active');
    if (item_add_on_click != 0) {
        this.addItemToList();
    }

}

Consumption.prototype.fillPieceHtml = function (numId, priceIndex) {
    var objSelectedItem = Menu.tblItemList[numId];

    this.numActiveItemId = numId;
    this.$objPiece.find('input, button').removeAttr('disabled');
    this.$objPiece.find('.item-name').text(objSelectedItem.itemName);

    if (objSelectedItem.itemMoreType != 0) {
        var itemPrice = objSelectedItem.itemPrice[priceIndex];
    } else {
        var itemPrice = objSelectedItem.itemPrice;
    }
    if (itemPrice.unit_piece != '' && itemPrice.price_units != '')
        this.$objPiece.find('.piece-units').attr('data-index', priceIndex).text('(' + itemPrice.unit_piece + itemPrice.price_units + ')');
    else
        this.$objPiece.find('.piece-units').attr('data-index', '-1').text('');

    this.$objPiece.find('input').focus();
}

Consumption.prototype.emptyingPieceHtml = function () {
    if (this.numActiveItemId) {
        this.numActiveItemId = null;
        this.$objPiece.find('input').val(1);
        this.$objPiece.find('input, button').attr('disabled', true);

        this.$objPiece.find('.item-name, .piece-units').text('');
        this.$objPiece.find('.piece-units').attr('data-index', '-1');
    }
}

Consumption.prototype.deleteItemFromList = function (numConsItemId) {
    var self = this;
    if (!numConsItemId) return;

    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_WARNING
        , title: 'Termék törlése'
        , message: 'Biztos törli a terméket a fogyasztási listából?<br><b>A törlés nem visszavonható!</b>'
        , buttons: [{
            label: 'Törlés'
            , icon: 'fa fa-trash'
            , hotkey: 13
            , action: function (dialogRef) {
                deleteItem(numConsItemId);
                dialogRef.close();
            }
        }, {
            label: 'Mégsem'
            , hotkey: 27
            , action: function (dialogRef) {
                dialogRef.close();
            }
        }]
        , modal: true
        , closable: false
    });

    function deleteItem(numItemId) {
        Easy.GW('consumption->deleteConsumptionItem', {
            cons_item_id: numItemId
        }).done(function (JSONResponse) {
            if (JSONResponse.data.success == 1) {
                delete self.objParentTable.tblConsumptionList[numItemId];
                self.$objList.find('li[data-id="' + numItemId + '"]').remove();
                self.objParentTable.refreshDraw();
                self.refreshSum();
            } else {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER
                    , title: 'Hiba történt'
                    , message: JSONResponse.data.msg
                });
            }
        });
    }
}

Consumption.prototype.updateItemToList = function (numConsItemId, numNewPiece) {
    var self = this;

    Easy.GW('consumption->modifyItemPiece', {
        cons_item_id: numConsItemId
        , piece: numNewPiece
    }).done(function (JSONResponse) {
        if (JSONResponse.data.success == 1) {
            self.objParentTable.tblConsumptionList[JSONResponse.data.item.id] = JSONResponse.data.item;
            var objItemPrice = JSON.parse(JSONResponse.data.item.price);
            var numConsItemSumPrice = Math.round(objItemPrice.price * numNewPiece);
            var strConsItemPrice = numConsItemSumPrice + '<span>Ft</span>';
            self.$objList.find('li[data-id="' + JSONResponse.data.item.id + '"] .cons-item-price').attr('data-sum-price', numConsItemSumPrice).html(strConsItemPrice);
            self.refreshSum();
        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });
}

Consumption.prototype.addItemToList = function () {
    var self = this;

    Easy.GW('consumption->addItemToList', {
        consumption_id: this.objParentTable.tblConsumptionData.id
        , item_id: this.numActiveItemId
        , item_piece: this.$objPiece.find('input').val()
        , item_price_index: this.$objPiece.find('.piece-units').attr('data-index')
    }).done(function (JSONResponse) {
        if (JSONResponse.data.success == 1) {


            self.objParentTable.tblConsumptionList[JSONResponse.data.item.id] = JSONResponse.data.item;
            var strConsItem = self.getConsListItem(JSONResponse.data.item, false);
            // if (self.$objList.find('li[data-id="'+JSONResponse.data.item.id+'"]').length > 0){
            // 	self.$objList.find('li[data-id="'+JSONResponse.data.item.id+'"]').replaceWith(strConsItem);
            // } else {
            self.$objList.append(strConsItem);
            // }
            self.initPieceListener();
            self.refreshSum();
            self.objParentTable.refreshDraw();


        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });
}

Consumption.prototype.getItemListSkeleton = function () {
    var self = this;
    var strConsumptionList = '<ul class="consuption-list-select">';
    $.each(this.objParentTable.tblConsumptionList, function (i, e) {
        strConsumptionList += self.getConsListItem(e, true);
    });
    strConsumptionList += '</ul>';
    return strConsumptionList;
}

/* Beállítjuk a bár osztálynál hogy az asztalt ki fogjuk választani.
 * Így az asztalokon a következő click eseményre a kiválasztás fog visszatérni
 * nem a fogyasztési lista nyílik meg.
 */
Consumption.prototype.sitOver = function (isFull) {
    this.isSelectPart = isFull;
    Bar.consSitoverSelectTable(this);
}

// Az asztal kiválasztása után rákérdezünk és mentjük adatbázisba a változásokat
Consumption.prototype.sitOverThere = function (objTable) {
    Bar.objConsumptionSelector = null;
    var isFull = this.isSelectPart;
    delete this.isSelectPart;

    var self = this,
        strMsg = ''
    strButtonText = '';

    if (!isFull) {
        strMsg = 'Kérem válassza ki a listából a termékeket amik át szeretne helyezni a ' + objTable.tableName + ' asztalhoz.';
        strMsg += self.getItemListSkeleton();
        strButtonText = 'Áthelyezés másik asztalhoz';
    } else {
        strMsg = 'Biztos mindent át kiván helyezni a ' + objTable.tableName + ' asztalhoz?';
        strButtonText = 'Áthelyezés';
    }

    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_WARNING
        , title: 'Fizetés'
        , message: strMsg
        , buttons: [{
            label: strButtonText
            , icon: 'fa fa-share'
            , action: function (dialogRef) {
                sitOverConsumptionList(!isFull, function () {
                    dialogRef.close();
                });
            }
        }, {
            label: 'Mégsem'
            , hotkey: 27
            , action: function (dialogRef) {
                dialogRef.close();
                self.init();
            }
        }]
        , modal: true
        , closable: false
        , onshown: function () {
            if (!isFull) {
                self.initPieceListener();
            }
        }
    });

    function sitOverConsumptionList(isPart, callback) {
        var tblAjaxData = {
            cons_id: self.objParentTable.tblConsumptionData.id
            , to_table_id: objTable.tableId
        };
        if (!isPart) {
            tblAjaxData.is_part = isPart;
        } else {
            tblAjaxData.is_part = isPart;
            tblAjaxData.items = self.getConsListSelectItem();
            if (tblAjaxData.items.length == 0) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER
                    , title: 'Nincs termék kiválasztva'
                    , message: 'Kérem válassza ki azokat a termékeket amit Át szeretne helyezni a másik asztalhoz!'
                });
                return false;
            }
        }

        Easy.GW('consumption->sitOverConsumption', {
            sit_over_data: tblAjaxData
        }).done(function (JSONResponse) {
            if (JSONResponse.data.success == 1) {
                // Végrehaljtjuk a nézet beli módosításokat

                setTablesConsumptionList(tblAjaxData, JSONResponse.data.tblConsumption);
            } else {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER
                    , title: 'Hiba történt'
                    , message: JSONResponse.data.msg
                });
            }

            if (typeof callback == 'function') {
                callback(JSONResponse.data);
            }
        });
    }

    function setTablesConsumptionList(objData, tblResults) {
        if (!objData.is_part) {
            // Ha az egész foglalási lista átadásra kerül, töröljök a régi asztal foglalási listályát és hozzáadjuk au új asztalhoz

            if (!objTable.tblConsumptionData) {
                // Ha nem ülnek még az asztalnál átadjuk az új asztalnak a foglalási listát
                objTable.tblConsumptionData = self.objParentTable.tblConsumptionData;
                objTable.tblConsumptionList = self.objParentTable.tblConsumptionList;
            } else {
                // Ha ülnek még az asztalnál átadjuk a termékeket a másik fogalási listához
                objTable.tblConsumptionData.guest_name += ' - ' + self.objParentTable.tblConsumptionData.guest_name;
                $.each(self.objParentTable.tblConsumptionList, function (i, e) {
                    objTable.tblConsumptionList[i] = e;
                })
            }

            self.objParentTable.deleteConsumption();
            objTable.refreshDraw();

        } else {
            // Ha a foglalási lista egy része kerül átadásra

            objTable.tblConsumptionData = tblResults.new_cons_head;
            objTable.tblConsumptionList = {};
            self.objParentTable.tblConsumptionList = {};

            // Hozzáadjuk a választott asztalhoz a foglalási lista elemeit
            $.each(tblResults.new_cons_list, function (i, e) {
                objTable.tblConsumptionList[e.id] = e;
            });

            // Hozzáadjuk a régi asztalhoz foglalási lista elemeit
            $.each(tblResults.old_cons_list, function (i, e) {
                self.objParentTable.tblConsumptionList[e.id] = e;
            });

            self.objParentTable.refreshDraw();
            objTable.refreshDraw();
        }

    }
}

Consumption.prototype.payment = function (isFull, parentDialogRef) {
    var self = this,
        strMsg = '',
        tblButtons = [{
            label: 'Mégsem'
            , hotkey: 27
            , action: function (dialogRef) {
                dialogRef.close();
            }
        }];

    if (Object.keys(self.objParentTable.tblConsumptionList).length > 0) {
        if (!isFull) {
            strMsg = 'Kérem válassza ki a listából a termékeket amik kifizetésre kerülnek.';
            strMsg += self.getItemListSkeleton();
            tblButtons.unshift({
                label: 'Fizetés'
                , icon: 'fa fa-money'
                , action: function (dialogRef) {
                    dialogRef.close();
                    setCloseConsumptionList(true, function () {
                        parentDialogRef.close();
                    });
                }
            });
        } else {
            strMsg = 'Biztos le kívánja zárni a fogyasztási listát az asztalnál?';
            tblButtons.unshift({
                label: 'Fizetés'
                , icon: 'fa fa-money'
                , action: function (dialogRef) {
                    dialogRef.close();
                    setCloseConsumptionList(false, function () {
                        parentDialogRef.close();
                    });
                }
            });
        }
    } else {
        strMsg = 'Az asztalnál még nem fogyasztottak semmit. Kívánja lezárni a fogyasztási listát?';
        tblButtons.unshift({
            label: 'Lezárás'
            , action: function (dialogRef) {
                dialogRef.close();
                setCloseConsumptionList(false, function () {
                    parentDialogRef.close();
                });
            }
        });
    }

    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_WARNING
        , title: 'Fizetés'
        , message: strMsg
        , buttons: tblButtons
        , modal: true
        , closable: false
        , onshown: function () {
            if (!isFull) {
                self.initPieceListener();
            }
        }
    });

    function setCloseConsumptionList(isPart, callback) {
        var tblAjaxData = {
            cons_id: self.objParentTable.tblConsumptionData.id
        };
        if (!isPart) {
            tblAjaxData.is_part = isPart;
        } else {
            tblAjaxData.is_part = isPart;
            tblAjaxData.items = self.getConsListSelectItem();
            if (tblAjaxData.items.length == 0) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER
                    , title: 'Nincs termék kiválasztva'
                    , message: 'Kérem válassza ki azokat a termékeket amit ki szeretne fizetni!'
                });
                return false;
            }
        }

        // console.log(tblAjaxData);

        // Bekérni az összeget először
        // UTÁNA lehet "Fizetni"
        Easy.GW('consumption->getPaymentInfo', {
            pay_data: tblAjaxData
        }).done(function (JSONResponse) {
            if (JSONResponse.data && JSONResponse.data.success == 1) {
                showSummaryDialog(JSONResponse.data.summary, tblAjaxData, callback);
            } else {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER
                    , title: 'Hiba történt'
                    , message: JSONResponse.data && JSONResponse.data.msg ? JSONResponse.data.msg : 'Ismeretlen hiba'
                });
            }
        });

    }

    function removeConsumptionList(removeConsData) {
        if (!removeConsData.is_part) {
            // Töröljük az asztal foglalási listáját
            self.objParentTable.deleteConsumption();
            delete self;
        } else {
            // Töröljük az elemeket az asztal foglalási listájából
            $.each(removeConsData.items, function (i, objItem) {
                var numDelItemId = objItem.id;
                var numDelItemIPiece = objItem.count;
                var numItemPiece = self.objParentTable.tblConsumptionList[numDelItemId].piece;
                if (numItemPiece <= numDelItemIPiece) {
                    delete self.objParentTable.tblConsumptionList[numDelItemId];
                } else {
                    self.objParentTable.tblConsumptionList[numDelItemId].piece = Math.round(numItemPiece - numDelItemIPiece);
                }
            });
            self.objParentTable.refreshDraw();
        }
    }

    function payConsumptionList(tblData, callback) {
        Easy.GW('consumption->payConsumption', {
            pay_data: tblData
        }).done(function (JSONResponse) {
            if (!JSONResponse.data || JSONResponse.data.success != 1) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER
                    , title: 'Hiba történt'
                    , message: JSONResponse.data && JSONResponse.data.msg ? JSONResponse.data.msg : 'Ismeretlen hiba'
                });
                return;
            }

            removeConsumptionList(tblData);

            if (typeof callback == 'function') {
                callback(JSONResponse);
            }
        });
    }

    function showSummaryDialog(numSumPrice, tblData, callback) {
        if (numSumPrice > 0) {
            var strHtml = ''; //'<p><strong>Fizetendő összeg: </strong></p>'; //Sikeresen rögzítettük a fizetési igényt.<br>
            strHtml += '<div class="sum-item-row head"> \
				<h4 class="sum-item-name">Termék neve</h4>\
				<div class="sum-item-piece">Mennyiség</div>\
				<div class="sum-item-price">Egységár</div>\
				<div class="sum-item-sumprice">Összesen</div>\
			</div>';
            if (!tblData.is_part) {
                $.each(self.objParentTable.tblConsumptionList, function (i, e) {
                    var objPrice = JSON.parse(e.price);
                    strHtml += '<div class="sum-item-row"> \
						<h4 class="sum-item-name">'+ e.name + '</h4>\
						<div class="sum-item-piece">'+ e.piece + ' db</div>\
						<div class="sum-item-price">'+ objPrice.price + ' Ft</div>\
						<div class="sum-item-sumprice">'+ (e.piece * objPrice.price) + ' Ft</div>\
					</div>';
                });
            } else {
                $.each(tblData.items, function (i, e) {
                    var consItem = self.objParentTable.tblConsumptionList[e.id]
                    var objPrice = JSON.parse(consItem.price);
                    strHtml += '<div class="sum-item-row"> \
						<h4 class="sum-item-name">'+ consItem.name + '</h4>\
						<div class="sum-item-piece">'+ parseInt(e.count) + ' db</div>\
						<div class="sum-item-price">'+ objPrice.price + ' Ft</div>\
						<div class="sum-item-sumprice">'+ (parseInt(e.count) * objPrice.price) + ' Ft</div>\
					</div>';
                });
            }
            strHtml += '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összesen:</span>' + numSumPrice + ' <small>Ft</small></div>';
            strHtml += '<div class="input-group float-left w-100 p-2"> \
							<div class="btn-group btn-group-toggle d-flex w-100" data-toggle="buttons"> \
							  <label class="btn btn-danger active w-50"> \
							  	<input type="radio" id="pay_money" name="paytype" value="0" autocomplete="off" checked="checked"> <span class="fa fa-money"></span> Készpénz \
							  </label> \
							  <label class="btn btn-danger w-50"> \
							    <input type="radio" id="pay_card" name="paytype" value="1" autocomplete="off"> <span class="fa fa-credit-card"></span> Bankkártya \
							  </label> \
							</div> \
						</div>';

            strHtml += '<div class="input-group float-left w-100 p-2"> \
							<input id="amount" type="text" placeholder="Fizetett összeg" autocomplete="off" class="form-control input-number"><span class="input-group-addon">Ft</span> \
						</div>';

            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_SUCCESS
                , title: 'Fogyasztás lezárása'
                , message: strHtml
                , closable: false
                , buttons: [
                    {
                        label: 'Rögzítés'
                        , icon: 'fa fa-check'
                        , cssClass: 'btn-success'
                        , hotkey: 13
                        , action: function (dialogRef) {

                            tblData.paid_amount = $('#amount').val();
                            tblData.card = $('#pay_card').is(':checked') ? 1 : 0;

                            if (parseInt(numSumPrice) > parseInt(tblData.paid_amount)) {
                                BootstrapDialog.show({
                                    type: BootstrapDialog.TYPE_DANGER
                                    , title: 'Hiba!'
                                    , message: 'A fizetett összeg nem lehet kevesebb, mint a fizetendő összeg!'
                                });
                            } else {

                                if (parseInt(numSumPrice) * 1.2 < parseInt(tblData.paid_amount)) {
                                    BootstrapDialog.show({
                                        type: BootstrapDialog.TYPE_DANGER
                                        , title: 'Figyelmeztetés!'
                                        , message: 'A borravaló összege nagyobb, mint 20%. Biztos benne, hogy megfelelő összeget ütött be?'
                                        , closable: false
                                        , buttons: [
                                            {
                                                label: 'Biztos'
                                                , icon: 'fa fa-check'
                                                , cssClass: 'btn-success'
                                                , hotkey: 13
                                                , action: function (dRef) {
                                                    payConsumptionList(tblData, function (JSONResponse) {

                                                        dRef.close();
                                                        dialogRef.close();
                                                        var SD = BootstrapDialog.show({
                                                            type: BootstrapDialog.TYPE_SUCCESS
                                                            , title: 'Sikeres fizetés'
                                                            , message: 'A fiztés sikeresen lezárult'
                                                            , onshown: function () {
                                                                setTimeout(function () {
                                                                    SD.close();
                                                                }, 1000);
                                                            }
                                                        });

                                                        if (typeof callback == 'function') {
                                                            callback(JSONResponse.data);
                                                        }
                                                    });


                                                }
                                            }, {
                                                label: 'Ellenőrzöm'
                                                , hotkey: 27
                                                , action: function (dRef) {
                                                    dRef.close();
                                                }
                                            }
                                        ]
                                    });

                                    return false;
                                } else {
                                    payConsumptionList(tblData, function (JSONResponse) {
                                        dialogRef.close();
                                        var SD = BootstrapDialog.show({
                                            type: BootstrapDialog.TYPE_SUCCESS
                                            , title: 'Sikeres fizetés'
                                            , message: 'A fiztés sikeresen lezárult'
                                            , onshown: function () {
                                                setTimeout(function () {
                                                    SD.close();
                                                }, 1000);
                                            }
                                        });

                                        if (typeof callback == 'function') {
                                            callback(JSONResponse.data);
                                        }
                                    });
                                }

                            }
                        }
                    },
                    {
                        label: 'Bezárás'
                        , hotkey: 27
                        , action: function (dialogRef) {
                            dialogRef.close();
                        }
                    }
                ]
                , onshown: function () {
                    // $('#amount').focus();

                    $('#amount').keypad({
                        keypadOnly: false
                        , showAnim: 'slide'
                        , showOptions: { direction: 'up' }
                        , duration: 'fast'
                        , layout: ['123' + $.keypad.CLOSE, '456' + $.keypad.CLEAR, '789', '0']
                    });
                }
            });
        } else {
            tblData.paid_amount = 0;
            Easy.GW('consumption->payConsumption', {
                pay_data: tblData
            }).done(function (JSONResponse) {
                if (!JSONResponse.data || JSONResponse.data.success != 1) {
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_DANGER
                        , title: 'Hiba történt'
                        , message: JSONResponse.data && JSONResponse.data.msg ? JSONResponse.data.msg : 'Ismeretlen hiba'
                    });
                    return;
                }

                removeConsumptionList(tblData);
                if (typeof callback == 'function') {
                    callback(JSONResponse.data);
                }
            });
        }
    }
}