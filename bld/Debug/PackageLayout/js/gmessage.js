/* Messages */
function errorMessage(text) {

	var eMsg = new $.Zebra_Dialog(text, {
		'type': 'error',
		'title': EasyText.error_occured,
		'buttons': [{
			caption: 'OK', 
			callback: function() { 
				eMsg.close();
			}
		}],
		'onClose':  function(caption) {

		},
		'keyboard': true,
		'width': !is_mobile ? 'auto' : $(window).width()-20,
		'modal': true,
		'auto_close': 3000,
		'show_close_button': true,
		'reposition_speed': 100
	});
	
	return false;

}

function alertMessage(text, title) {

	var aMsg = new $.Zebra_Dialog(text, {
		'type': 'warning',
		'title': title,
		'buttons': [{
			caption: 'OK', 
			callback: function() { 
				aMsg.close();
			}
		}],
		'onClose':  function(caption) {

		},
		'keyboard': true,
		'width': !is_mobile ? 'auto' : $(window).width()-20,
		'modal': true,
		'auto_close': 5000,
		'show_close_button': true,
		'reposition_speed': 100
	});
}

function infoMessage(text, title) {

	var iMsg = new $.Zebra_Dialog(text, {
		'type': 'information',
		'title': title,
		'buttons': [{
			caption: 'OK', 
			callback: function() { 
				iMsg.close();
			}
		}],
		'onClose':  function(caption) { },
		'keyboard': true,
		'width': !is_mobile ? 'auto' : $(window).width()-20,
		'modal': true,
		'auto_close': 5000,
		'show_close_button': true,
		'reposition_speed': 100
	});

}

function fnMessage(text, title, action, init) {

	if (title == "") {
		title = EasyText.information;
	}

	var fMsg = new $.Zebra_Dialog(text, {
		'type': 'none',
		'title': title,
		'buttons': [{
			caption: EasyText.accept, 
			callback: function() { 
				action();
				fMsg.close();
			}
		},
		{
			caption: EasyText.cancel,
			callback: function() {
				fMsg.close();
			}
		}],
		'onClose':  function(caption) {
			fMsg.close();
		},
		'width': !is_mobile ? 'auto' : $(window).width()-20,
		'keyboard': true,
		'modal': true,
		'show_close_button': true,
		'reposition_speed': 100
	});

	if ( typeof init != 'undefined')
		init();

	return false;
}

