/*
 * Ezt az osztály az egyes helyszínek módosításáért felelős
 */
var PlaceSettings = function(obj){
	this.place = obj;

	this.init();
}

PlaceSettings.prototype.init = function(){
	this.$objPlaceSetting = $('<div class="place-checkbox"></div>');
	this.$objPlaceSettingName = $('<h3>'+this.place.name+'</h3>');
	this.$objPlaceStatus = $('<span class="place-status">'+(this.place.status!='0'?'Aktív':'Inaktív')+'</span>');
	this.$objPlaceSettingActivation = $('<input type="checkbox" id="placeActivation_'+this.place.id+'" class="js-switch"'+(this.place.status!='0'?' checked':'')+'/>');
	this.$objPlaceSettingButton = $('<button class="btn btn-sm pull-right btn-dark"><span class="fa fa-cog"></span> Szerkesztés</button>');
	this.$objPlaceSettingDel = $('<button class="btn btn-sm pull-right ml-1 btn-danger"><span class="fa fa-trash"></span> Törlés</button>');

	this.$objPlaceSetting.append(this.$objPlaceSettingName, this.$objPlaceStatus, this.$objPlaceSettingDel, this.$objPlaceSettingButton, this.$objPlaceSettingActivation);
	$('#settingPlaces').append(this.$objPlaceSetting);

	this.setActivationButton();
	this.placeSettingsListener();
}

PlaceSettings.prototype.setName = function(strNewName){
	this.$objPlaceSettingName.text(strNewName);
}

PlaceSettings.prototype.setActivationButton = function(){
	var element = document.getElementById('placeActivation_'+this.place.id);
	this.switchery = new Switchery(element, {
		color             	: '#12a233'
		, secondaryColor    : '#e5e5e5'
		, jackColor         : '#fff'
		, jackSecondaryColor: null
		, className         : 'switchery'
		, disabled          : false
		, disabledOpacity   : 0.5
		, speed             : '0.6s'
		, size              : 'default'
	});
}

PlaceSettings.prototype.placeSettingsListener = function(){
	var self = this;

	this.$objPlaceSettingActivation.change(function(){
		var isActive = $(this).is(':checked');
		changePlaceStatus(isActive);
	});

	this.$objPlaceSettingButton.on('click', function(){
		Bar.editPlace(self.place.id);
	});

	this.$objPlaceSettingDel.on('click', function(){
		self.deletePlace();
	});

	function changePlaceStatus(isActive){
		Easy.GW('place->changePlaceStatus', {
			place_id: self.place.id
			, is_active: isActive
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success != 1){
				self.switchery.setPosition(true);
				BootstrapDialog.danger(JSONResponse.data.msg);
			} else {
				self.$objPlaceStatus.text(!isActive?'Inaktív':'Aktív');
				if (!isActive){
					Bar.destoyPlace(self.place.id);
				} else {
					Bar.initPlace(JSONResponse.data.place_data[0], true);
				}
			}
		});
	}
}

PlaceSettings.prototype.deletePlace = function(){
	var self = this;

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_WARNING
		, title: 'Hely törlése'
		, message: 'Biztos törölni kívánja a helyet?'
		, buttons: [{
			label: 'Törtés'
			, icon: 'fa fa-trash'
			, cssClass: 'btn-primary'
			, hotkey: 13
			, action: function(dialogRef){
				deletePlaceData();
				dialogRef.close();
			}
		}, {
			label: 'Mégsem'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
	});

	function deletePlaceData(){
		Easy.GW('place->deletePlace', {
			place_id: self.place.id
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success == 1){
				self.$objPlaceSettingActivation.off('change');
				self.$objPlaceSettingButton.off('click');
				self.$objPlaceSettingDel.off('click');
				Bar.destoyPlace(self.place.id);
				self.$objPlaceSetting.remove();
				delete self;
			} else {
				BootstrapDialog.show({
					type: BootstrapDialog.TYPE_DANGER
					, title: 'Hiba történt'
					, message: JSONResponse.data.msg
				});
			}
		});
	}
}