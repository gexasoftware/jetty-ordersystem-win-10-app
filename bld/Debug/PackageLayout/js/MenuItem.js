/*
 * Ezt az menu kategória osztálya
 */
var MenuItem = function(objData, objParent){
	if (!objData || !objData.id) return false;

	this.itemId = objData.id;
	this.itemCategoryId = objData.category_id;
	this.parentCategory = objParent;
	this.itemName = objData.name;
	this.itemMoreType = objData.more_type;
	this.itemPrice = typeof objData.price == 'string' ? JSON.parse(objData.price) : objData.price;
	this.itemOptions = typeof objData.options == 'string' ? JSON.parse(objData.options) : objData.options;

	this.init();
	this.ItemListener();
}

MenuItem.prototype.init = function(){
	this.$item = $('<li class="menu-item" data-id="' + this.itemId + '"/>');
	this.$itemName = $('<h5 class="pull-left"></h5>');
	this.$itemPrice = $('<div class="price pull-right"></div>');
	this.$itemDel = $('<button class="btn btn-sm pull-right ml-1 btn-danger"><span class="fa fa-trash"></span> Törlés</button>');
	this.$itemOptions = $('<button class="btn btn-sm pull-right btn-dark"><span class="fa fa-cog"></span> Szerkesztés</button>');

	this.parentCategory.$categoryList.append(this.$item);
	this.$item.append(this.$itemName, this.$itemDel, this.$itemOptions, this.$itemPrice);
	this.ItemDrawData();
}

MenuItem.prototype.ItemDrawData = function(){
	this.$itemName.text(this.itemName);
	var strHtml = '';
	if (this.itemMoreType != 1){
		strHtml += '<span><strong>'+this.itemPrice.price+' Ft</strong>'+(this.itemPrice.price_units != '' ? ' / '+this.itemPrice.unit_piece+this.itemPrice.price_units  : '')+'</span>';
	} else {
		$.each(this.itemPrice, function(i,e){
			strHtml += '<span><strong>'+e.price+' Ft</strong>'+(e.price_units != '' ? ' / '+e.unit_piece+e.price_units  : '')+'</span>';
		});
	}
	this.$itemPrice.html(strHtml);
}

MenuItem.prototype.ItemListener = function(){
	var self = this;

	this.$itemDel.on('click', function(){
		self.delete();
	});

	this.$itemOptions.on('click', function(){
		self.parentCategory.EditMenuItem(self.itemId);
	});
}

MenuItem.prototype.refreshData = function(tblNewData){
	this.itemName = tblNewData.name;
	this.itemPrice = tblNewData.price;
	this.itemMoreType = tblNewData.more_type;
	this.ItemDrawData();
}

MenuItem.prototype.delete = function(){
	var self = this;

	var confirmeMessage = 'Biztos benne hogy törli a terméket?';

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_WARNING
		, title: 'Termék törlése'
		, message: confirmeMessage
		, buttons: [{
			label: 'Törlöm'
			, icon: 'fa fa-trash'
			, cssClass: 'btn-warning'
			, hotkey: 13
			, action: function(dialogRef){
				DeleteItem();
				dialogRef.close();
			}
		},{
			label: 'Mégsem'
			, icon: 'fa fa-remove'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
	});

	function DeleteItem(){
		Easy.GW('menu->deleteMenuItem', {
			item_id: self.itemId
		}).done(function(JSONResponse) {
			if (JSONResponse.data != null){
				if (JSONResponse.data.success != 1){
					BootstrapDialog.danger('A termék törlése meghiúsult.');
				} else {
					self.$itemDel.off('click');
					self.$itemOptions.off('click');

					self.$item.remove();
					delete self.parentCategory.items[self.itemId];
				}
			}
		});
	}
}