function padLeft(str, length, padder) {
	if (str.length >= length) {
		return str;
	}
	while (str.length < length) {
		str = padder + str;
	}

	return str;
}

var Session = {};

Session.init = function(restart, callback) {
	if (typeof callback !== 'function') return false;

	if (restart && !window.session) restart = 0;

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_SUCCESS
		, title: (restart != 1 ? 'Munkamenet kezdése' : 'Új munkamenet indítása')
		, message: (restart != 1 ? '<h1>ZÁRVA!</h1><br>Elindít egy új munkamenetet?' : '<strong>Új munkamenetet kezd?</strong><br>Ezzel az előző munkamenetet lezárja és újat hoz létre! <strong>Folytatja?</strong>')
		, buttons: [{
			label: 'Indítás'
			, icon: 'fa fa-play'
			, cssClass: 'btn-success'
			, hotkey: 13
			, action: function(dialogRef){
				Easy.GW('session->start', {
					a: Math.random(),
					restart: restart
				}).done(function(JSONData) {
					if (!JSONData.data || JSONData.data.success != 1) {
						alert('A munkamenet létrehozása közben hiba történt! Frissítse az oldalt!');
						return;
					}

					window.session = JSONData.data.s_id;
					var d = new Date();
                    window.sessionDate = d.getFullYear() + '-' + padLeft((d.getMonth() + 1).toString(), 2, '0') + '-' + padLeft(d.getDate().toString(), 2, '0');
                    console.log(restart);
                    return;

					if (restart != 0)  {
						var income = JSONData.data.income,
							strCloseContent = '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összes bevétel:</span>'+income.daily_total+' <small>Ft</small></div>';
						strCloseContent += '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összes borravaló:</span>'+income.daily_tip+' <small>Ft</small></div>';

						BootstrapDialog.show({
							type: BootstrapDialog.TYPE_SUCCESS
							, title: 'Zárás összesítő'
							, message: strCloseContent
							, buttons: [{
								label: 'Rendben'
								, icon: 'fa fa-refresh'
								, cssClass: 'btn-success'
								, hotkey: 13
								, action: function(dialogRef){
									window.location.reload(true);
								}
							}]
							, modal: true
							, closable: false
						});
                    }
                    if (typeof callback === 'function')
					    callback();
				});
				dialogRef.close();
			}
		},
		{
			label: 'Mégsem'
			, icon: 'fa fa-close'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
	});
}

Session.close = function() {
	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_WARNING
		, title: 'Zárás'
		, message: '<strong>Biztos zárja a napot?</strong><br>Ezzel a művelettel a munkamenetet lezárja! <strong>Folytatja?</strong>'
		, buttons: [{
			label: 'Bezárás'
			, icon: 'fa fa-close'
			, cssClass: 'btn-danger'
			, hotkey: 13
			, action: function(dialogRef){
				Easy.GW('session->close', {
					a: Math.random()
				}).done(function(JSONData) {
					if (!JSONData.data || JSONData.data.success != 1) {
						if (JSONData.data.message) {
							BootstrapDialog.show({
								type: BootstrapDialog.TYPE_DANGER
								, title: 'Hiba történt'
								, message: JSONData.data.message
							});
						}
						return;
					}

					var income = JSONData.data.income,
						strCloseContent = '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összes bevétel:</span>'+income.daily_total+' <small>Ft</small></div>';
					strCloseContent += '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összes borravaló:</span>'+income.daily_tip+' <small>Ft</small></div>';

					BootstrapDialog.show({
						type: BootstrapDialog.TYPE_SUCCESS
						, title: 'Zárás összesítő'
						, message: strCloseContent
						, buttons: [{
							label: 'Rendben'
							, icon: 'fa fa-refresh'
							, cssClass: 'btn-success'
							, hotkey: 13
							, action: function(dialogRef){
								window.location.reload(true);
							}
						}]
						, modal: true
						, closable: false
					});
				});

				dialogRef.close();
			}
		},
		{
			label: 'Mégsem'
			, cssClass: 'btn-default'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
	});
}

Session.showIncome = function() {
	var
		// d = new Date(),
		// actualDate = d.getFullYear() + '-' + padLeft((d.getMonth()+1).toString(), 2, '0') + '-'+padLeft(d.getDate().toString(), 2, '0');
		actualDate = window.sessionDate;

	var dateInput = '<div class="input-group float-left w-100 p-2"> \
						<input id="date" value="'+actualDate+'" type="date" placeholder="Válasszon dátumot!" autocomplete="off" class="form-control"> \
					</div>';

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_SUCCESS
		, title: 'Bevétel szűrése dátumra'
		, message: '<strong>Válassza ki a dátumot: </strong><br>'+dateInput
		, buttons: [{
			label: 'Bevétel mutatása'
			, icon: 'fa fa-money'
			, cssClass: 'btn-success'
			, hotkey: 13
			, action: function(dialogRef){
				Easy.GW('session->show_income', {
					date: $('#date').val(),
					a: Math.random()
				}).done(function(JSONData) {
					if (!JSONData.data || JSONData.data.success != 1) {
						if (JSONData.data.message) {
							BootstrapDialog.show({
								type: BootstrapDialog.TYPE_DANGER
								, title: 'Hiba történt'
								, message: JSONData.data.message
							});
						}
						return;
					}

					var income = JSONData.data.income,
						strCloseContent = '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összes bevétel:</span>'+income.daily_total+' <small>Ft</small></div>';
						strCloseContent += '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összes nem fizetett fogyasztás:</span>'+income.not_paid_items+' <small>Ft</small></div>';
						strCloseContent += '<div class="text-right h4 sum-item-summary float-left w-100 p-2"><span class="float-left">Összes borravaló:</span>'+income.daily_tip+' <small>Ft</small></div>';

					BootstrapDialog.show({
						type: BootstrapDialog.TYPE_SUCCESS
						, title: 'Bevétel összesítő'
						, message: strCloseContent
						, buttons: [{
							label: 'Rendben'
							, icon: 'fa fa-check'
							, cssClass: 'btn-success'
							, hotkey: 13
							, action: function(dialogRef){
								dialogRef.close();
							}
						}]
						, modal: true
						, closable: false
					});
				});

				dialogRef.close();
			}
		},
		{
			label: 'Mégsem'
			, cssClass: 'btn-default'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
	});
}
