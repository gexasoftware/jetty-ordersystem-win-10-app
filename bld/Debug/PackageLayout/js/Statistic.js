var Statistics = function (){
    this.startDate      = (typeof sessionDate != 'undefined') ? sessionDate : moment().format('YYYY-MM-DD');
    this.periodStart    = '';
    this.periodEnd      = '';

    this.isLogin        = false;

    this.initCharger();
    this.init();
}

Statistics.prototype.initCharger = function () {
    var self = this,
        minYear = 2018,
        strYearHtml = '';

    for (var y = new Date().getFullYear(); y >= minYear; y--) {
        strYearHtml += '<option value="' + y + '">' + y + '</option>';
    }

    $('#dailyYearSelect, #monthlyYearSelect').html(strYearHtml);

    $('#dailyDatePicker').flatpickr({
        maxDate: moment().format('YYYY-MM-DD')
        , defaultDate: this.startDate
    });

    $("#monthlyDatePickerStart").flatpickr({
        "plugins": [
            new rangePlugin({ input: "#monthlyDatePickerEnd" })
        ]
    });

    $('a[data-toggle="tab"][href="#reports"]').on('shown.bs.tab', function (e) {
        if (self.isLogin && (typeof sessionDate == 'undefined' || self.startDate == sessionDate))
            self.getDailyStatistic();
    });

    // $('a[data-toggle="tab"][href="#reports"]').on('hide.bs.tab', function (e) { });
}

Statistics.prototype.init = function () {
    var self = this;

    //google.charts.load('current', { packages: ['corechart', 'bar'] });

    $('#dailyDatePicker').on('change', function () {
        self.startDate = $(this).val();
        self.getDailyStatistic();
    });

    $('#monthlyDatePickerStart').on('change', function () {
        self.periodStart = $(this).val();
        self.periodEnd = $('#monthlyDatePickerEnd').val();
        if (self.periodEnd != '')
            self.getPeriodStatistic();
    });

    $('#login').on('submit', function (e) {
        e.preventDefault();
        self.loginStatistic();
    });

    $('#logoutToStatistics').on('click', function () {
        self.logoutStatistic();
    });

    var DateTimeFormats = {
        second: '%Y-%m-%d<br/>%H:%M:%S',
        minute: '%Y-%m-%d<br/>%H:%M',
        hour: '%Y-%m-%d<br/>%H:%M',
        day: '%Y<br/>%m-%d',
        week: '%Y<br/>%m-%d',
        month: '%Y-%m',
        year: '%Y'
    };
    var groupingUnits = [['week', [1]], ['month', [1, 2, 3, 4, 5]]];
}

Statistics.prototype.loginStatistic = function () {
    var self = this;
    var objLogName = $('#loginName');
    var logName = objLogName.val();
    var objLogPass = $('#loginPass');
    var logPass = objLogPass.val();

    var mameIsValid = validateInput(logName, objLogName);
    var passIsValid = validateInput(logPass, objLogPass);

    if (!mameIsValid || !passIsValid) {
        return false;
    }

    Easy.GW('statistic->loginStatistic', {
        log_name: logName
        , log_pass: logPass
    }).done(function (JSONResponse) {
        if (JSONResponse.data.success == 1) {
            if (JSONResponse.data.is_valid) {
                self.isLogin = true;
                objLogPass.removeClass('login-error');
                objLogName.val('');
                objLogPass.val('');
                $('#loginContent').hide();
                $('#reportContent, #logoutToStatistics').show();
                self.getDailyStatistic();
            } else {
                objLogPass.addClass('login-error');
            }
        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });

    function validateInput(needString, objElement) {
        if (needString == '') {
            objElement.removeClass('login-error');
            objElement.addClass('in-valid');
            return false;
        }

        objElement.removeClass('in-valid');
        return true;
    }

}

Statistics.prototype.logoutStatistic = function () {
    self.isLogin = false;
    $('#reportContent, #logoutToStatistics').hide();
    $('#loginContent').show();
    $('#dailyIncome, #dailyInfo, #periodItem, #periodIncome, #periodInfo').html('');
    $('#monthlyDatePickerEnd, #monthlyDatePickerStart').val('');
}

Statistics.prototype.getDailyStatistic = function(){
    var self = this;

    Easy.GW('statistic->getDailyStatistic', {
        date: this.startDate
    }).done(function(JSONResponse) {
        if (JSONResponse.data.success == 1){
            self.drawDailyStatistic(JSONResponse.data.statistic[self.startDate], JSONResponse.data.income);
        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });
}


Statistics.prototype.getPeriodStatistic = function(){
    var self = this;

    Easy.GW('statistic->getPeriodStatistic', {
        date_start: this.periodStart
        , date_end: this.periodEnd
    }).done(function(JSONResponse) {
        if (JSONResponse.data.success == 1){
            console.log(JSONResponse.data.statistic, JSONResponse.data.income);
        } else {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER
                , title: 'Hiba történt'
                , message: JSONResponse.data.msg
            });
        }
    });
}


Statistics.prototype.drawDailyStatistic = function(results, income) {
    var itemsData = [],
        products = [],
        data = [];

    if (!results)
        return;

    $.each(results, function (id, item) {
        products.push(item.cat_name);
        itemsData.push({ 
            'name': item.cat_name,
            'color': item.color,
            'y': item.sum
        });
        $.each(item.items, function(i, d) {
            // TOOLTIP
        });
    });

    // [{"name":"Cappuccino  (1 db x 390 Ft)","data":[1,null]},{"name":"Cocomon  (1 db x 590 Ft)","data":[1,null]},{"name":"Barackos mousse  (1 db x 450 Ft)","data":[null,1]},{"name":"Bársonyosálom - Munkácsy  (1 db x 550 Ft)","data":[null,1]},{"name":"Boci szelet  (1 db x 690 Ft)","data":[null,1]},{"name":"Diós kosár  (1 db x 300 Ft)","data":[null,1]}]
    console.log(JSON.stringify(itemsData));

    var highchartsOptions = Highcharts.setOptions({
        lang: {
            loading: 'Betöltés...',
            xportButtonTitle: "Exportál",
            printButtonTitle: "Importál",
            downloadPNG: 'Letöltés PNG képként',
            downloadJPEG: 'Letöltés JPEG képként',
            downloadPDF: 'Letöltés PDF dokumentumként',
            downloadSVG: 'Letöltés SVG formátumban',
            downloadSVG: 'Letöltés SVG formátumban',
            resetZoom: "Visszaállít",
            resetZoomTitle: "Visszaállít",
            thousandsSep: " ",
            decimalPoint: ','
        },
        credits: { enabled: false },
        chart: { zoomType: 'xy' }
    });

    $('#dailyIncome').css({ width: '100%', height: 400 }).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Termékek szerinti fogyasztás'
        },
        xAxis: {
            categories: products,
            title: {
                text: 'Termék'
            }
        },
        yAxis: {
            title: {
                text: 'Teljes fogyasztás'
            }
        },
        series: [{
            name: 'Termék értékesítések (Ft)',
            data: itemsData
        }]
    });
}

/*

Statistics.prototype.drawDailyStatistic = function(tblData, tblSumIncome){
    var self = this;
    google.charts.setOnLoadCallback(drawDailyIncome);
    drawSumIncome();

    function drawDailyIncome() {
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Termékek');
        data.addColumn('number', 'Bevétel');
        data.addColumn({'type': 'string', 'role': 'style' });
        data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

        var rowData = [];
        if (typeof tblData != 'undefined')
            $.each(tblData, function(i, e){
                rowData.push([e.cat_name, e.sum, e.color, self.getTooltipHtml(e.items, e.cat_name)]);
            });

        data.addRows(rowData);

        var options = {
            chart: {
                title: 'Napi bevétel',
                subtitle: 'A ' + self.startDate + ' napon keletkezett bevétel tételekre lebontva.',
            },
            height: 350,
            hAxis: {
                title: 'Termék'
            },
            vAxis: {
                title: 'Bevétel'
                , format: '#.### Ft'
            },
            legend: 'none',
            tooltip: { isHtml: true }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('dailyIncome'));
        chart.draw(data, options);
    }

    function drawSumIncome(){
        var strHtml = '';
        strHtml += '<ul class="statistic-info">';
        strHtml += '<li class="income">Összes bevétel: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.total) + ' Ft</strong></li>';
        if (tblSumIncome.not_paid_items != 0)
            strHtml += '<li class="not-pay">Összes nem fizetett fogyasztás: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.not_paid_items) + ' Ft</strong></li>';

        strHtml += '<li class="tip">Összes borravaló: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.tip) + ' Ft</strong></li>';
        strHtml += '</ul>';

        $('#dailyInfo').html(strHtml);
    }

    $(window).resize(function (){
        drawDailyIncome();
    });
}

Statistics.prototype.getTooltipHtml = function(tblItems, strCatName){
    var numSum = 0;
    var strHtml = '<div class="statistic-tooltip">';
    strHtml += '<h2>' + strCatName + '</h2>';

    strHtml += '<ul class="tooltip-list">';
    $.each(tblItems, function(numItemId, objItem){
        strHtml += '<li>' + objItem.name + (objItem.unit != '' ? ' (' + objItem.unit + ')' : '') + ': <strong>' + objItem.piece + ' * ' + new Intl.NumberFormat('hu-HU').format(objItem.price) + ' Ft</strong></li>';
        numSum += parseInt(objItem.piece * objItem.price);
    });
    strHtml += '</ul>';
    strHtml += '<div class="sum">Összesen: <span>' + new Intl.NumberFormat('hu-HU').format(numSum) + ' Ft</span></div>';
    strHtml += '</div>';

    return strHtml;
}

Statistics.prototype.drawPeriodStatistic = function(tblData, tblSumIncome){
    var self = this;
    google.charts.setOnLoadCallback(drawPeriodIncome);
    google.charts.setOnLoadCallback(drawPeriodItem);
    drawSumIncome();

    function drawPeriodIncome(){
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Napok');
        data.addColumn('number', 'Bevétel');

        var rowData = [];
        var date = self.periodStart;

        while (date <= self.periodEnd) {
            rowData.push([date, 0]);
            date = moment(date, 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD');
        }

        $.each(tblData, function(i, e){
            var sum = 0;
            $.each(e, function(ei, ee){
                sum += ee.sum;
            });

            rowData = jQuery.map( rowData, function( tblData, index ) {
                if (tblData[0] == i)
                    tblData[1] = sum;
                return [tblData];
            });
        });

        data.addRows(rowData);

        var options = {
            height: 350,
            hAxis: {
                title: 'Nap'
            },
            vAxis: {
                title: 'Bevétel'
                , minValue: 0
                , format: '#.### Ft'
            }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('periodIncome'));
        chart.draw(data, options);
    }

    function drawPeriodItem(){
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Napok');
        data.addColumn('number', 'Termékek');
        data.addColumn({'type': 'string', 'role': 'style' });
        data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

        var tblItems = {};

        $.each(tblData, function(i, e){
            $.each(e, function(catId, objCat){
                if (typeof tblItems[catId] == 'undefined'){
                    tblItems[catId] = objCat;
                } else {
                    tblItems[catId].sum += objCat.sum;
                    $.each(objCat.items, function(itemId, objItem){
                        if (typeof tblItems[catId].items[itemId] == 'undefined'){
                            tblItems[catId].items[itemId] = objItem;
                        } else {
                            tblItems[catId].items[itemId].piece += objItem.piece;
                        }
                    });
                }
            });
        });

        var rowData = [];
        $.each(tblItems, function(i, e){
            rowData.push([e.cat_name, e.sum, e.color, self.getTooltipHtml(e.items, e.cat_name)]);
        });
        data.addRows(rowData);

        var options = {
            height: 350,
            hAxis: {
                title: 'Termék'
            },
            vAxis: {
                title: 'Bevétel'
                , format: '#.### Ft'
            },
            legend: 'none',
            tooltip: { isHtml: true }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('periodItem'));
        chart.draw(data, options);
    }

    function drawSumIncome(){
        var strHtml = '';
        strHtml += '<ul class="statistic-info">';
        strHtml += '<li class="income">Összes bevétel: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.total) + ' Ft</strong></li>';
        if (tblSumIncome.not_paid_items != 0)
            strHtml += '<li class="not-pay">Összes nem fizetett fogyasztás: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.not_paid_items) + ' Ft</strong></li>';

        strHtml += '<li class="tip">Összes borravaló: <strong>' + new Intl.NumberFormat('hu-HU').format(tblSumIncome.tip) + ' Ft</strong></li>';
        strHtml += '</ul>';

        $('#periodInfo').html(strHtml);
    }

    $(window).resize(function (){
        drawPeriodIncome();
    });
}
*/



var Statistics = new Statistics();