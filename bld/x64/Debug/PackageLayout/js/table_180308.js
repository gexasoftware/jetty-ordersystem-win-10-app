var Table = {};

Table.init = function() {
	$('#desktops').css({
		height: ($(window).height() - $('nav:first').height() - 115)
	});
	$('.table').each(function() {
		var dim = $(this).data('dimensions').split(',');
		var obj = {};
		$.each(dim, function(i,e) {
			e = e.split(':');
			obj[e[0]] = parseInt(e[1]);
		});
		//console.log(obj);
		$(this).css({
			position: 'absolute',
			background: 'black',
			width: obj.w,
			height: obj.h,
			top: obj.y,
			left: obj.x,
			cursor: 'pointer'
		});
	});

	$(document).on('click', '.table', function() {
		if (Table.editing != true) {
			Table.click($(this));
		}
	});
}

Table.editing = false;
Table.edit = function(btn) {
	Table.editing = true;
	$('.table').draggable({
		cursor: "crosshair"
	}).resizable({
		step: 10
	}).css({
		cursor: 'move',
		border: '2px dashed #999'
	});
	btn.addClass('btn-warning').removeClass('btn-success').html('<span class="fa fa-close"></span> Szerkesztés vége').attr('onclick', 'Table.endEdit($(this));');
}

Table.endEdit = function(btn) {
	if (!Table.editing) {
		return;
	}
	$('.table').draggable('destroy').css({cursor: 'pointer', border: 'none'});
	btn.addClass('btn-success').removeClass('btn-warning').html('<span class="fa fa-pencil"></span> Asztalok szerkesztése').attr('onclick', 'Table.edit($(this));');
	Table.editing = false;
}

Table.add = function(dimensions) {

}

Table.click = function(object) {
	infoMessage(object.prop('id'), 'Rendelés leadása');
}

$(function() {
	Table.init();
});