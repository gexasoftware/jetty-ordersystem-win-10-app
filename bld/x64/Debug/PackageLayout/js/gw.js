var url_base = 'http://localhost/OrderSystem/';

if (typeof (Easy) === 'undefined')
	var Easy = {};

(function($) {

	Easy.GW_Options = {
		url: url_base + '_gw_.php',
		token: false,
		dataType: 'json',
		type: 'post',
		showMessages: true
	};

    /**
     * Visszaküldött üzenetek megjelenítése
     */
     function displayMessages(messages, warnings, delay) {

         if (Easy.GW_Options.showMessages === true) {
     		$.each(messages || [], function(i, m) {
     			try {

     				switch (m['class']) {
     					case 'error':
     						console && console.log(m['text']);
     					break;
     					case 'warning':
     						console && console.log(m['text']);
     					break;
     					case 'info':
     						console && console.log(m['text']);
     					break;
     					case 'success':
     						console && console.log(m['text']);
     					break;
     					case 'tip':
     						console && console.log(m['text']);
     					break;
     				}

     			} catch (e) {
     				console && console.log(e.message);
     			}
     		});
     	}

     }

    /**
     * Paraméterek feldolgozása
     */
     function parse(action) {

		// Ha objektumot kap, akkor azzal már nem kell foglalkozni
		if (typeof action == 'object')
			return action;

		// Ha nem, akkor egy string és átalakítjuk
		var tmp = action.split('->');
		if (tmp.length == 2) {
			return {service: tmp[0], action: tmp[1]};
		} else
		if (tmp.length == 3) {
			return {module: tmp[0], service: tmp[1], action: tmp[2]};
		} else
		if (tmp.length == 4) {
			return {module_type: tmp[0], module: tmp[1], service: tmp[2], action: tmp[3]};
		}
	}

	Easy.GW = function(action, data) {

		if (!data)data = {};
		// visszatéréi deferred létrehozása
		var dfd = $.Deferred();
		var options = Easy.GW_Options;
		options.data = parse(action);
		options.data.token = false;
        options.data.data = data;
        //console.log(options);

		// Ajax kérés elküldése
        var jqx = $.ajax(options);

		// Visszatérések feldolgozása
		jqx.done(function(response) {
			var hasError = false,
                errors = [];
            
			if (typeof response == 'object') {

                $.each(response.messages || [], function (i, m) {
					if (m['class'] == 'error') {
						errors.push(m['text']);
						hasError = true;
					}
				});

				displayMessages(response.messages);
			}

			if (hasError) {
				dfd.reject.call(dfd, response /* response */, errors[0] /* first error */, errors /* error list */);
			} else {
				dfd.resolve.apply(dfd, arguments);
			}

		}).fail(function(jqXHR, statusText, error) {
            
			var errorMsg = '';
			switch (statusText) {
				case 'timeout':
					errorMsg = 'A hívás túllépte az időkorlátot.';
				break;
				case 'error':
					errorMsg = 'A hívás közben hiba épett fel: ' + error;
				break;
				case 'abort':
					errorMsg = 'A hívást megszakították.';
				break;
				case 'parsererror':
					errorMsg = 'A hívás hibás kimenettel tért vissza.';
				break;
				default:
					errorMsg = 'A hívás közben ismeretlen hiba lépett fel.';
			}

			var fake_json = {messages: [{'class': 'error', text: errorMsg}]};
			displayMessages(fake_json.messages);

			dfd.reject.call(dfd, fake_json, errorMsg, [errorMsg]);
		});

		return dfd.promise(jqx);
	}

	window.GW = $.extend(Easy.GW, {
		parse: parse
	});

})(jQuery);