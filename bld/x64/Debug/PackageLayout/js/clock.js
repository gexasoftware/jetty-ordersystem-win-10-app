var Clock = {
	_elm: null,
	ctx: null,
	radius: null,
	height: null,
	width: null,
	numberColor: '#FFF',
	background: '#343a40',
	init: function(container, width, height) {
		this.width = width;
		this.height = height;
		this._elm = document.createElement('canvas');
		this._elm.style.background = 'transparent';
		this._elm.style.zIndex = 2;
		this._elm.setAttribute('width', this.width);
		this._elm.setAttribute('height', this.height);
		this._elm.setAttribute('id', 'Clock');
		document.getElementById(container).appendChild(this._elm);

		this.ctx = this._elm.getContext('2d');

		var radius = this.height / 2;
		this.ctx.translate(radius, radius);
		this.radius = radius * 0.90

		var _t = this;
		setInterval(function() {
			_t.drawClock();
			$('#Clock').draggable();
		}, 1000);
	},

	drawClock: function() {
		this.drawFace();
		this.drawNumbers();
		this.drawTime();
	},

	drawFace: function() {
		var grad;
		this.ctx.beginPath();
		this.ctx.arc(0, 0, this.radius, 0, 2*Math.PI);
		this.ctx.fillStyle = this.background;
		this.ctx.fill();
		grad = this.ctx.createRadialGradient(0,0,this.radius*0.95, 0,0,this.radius*1.05);
		grad.addColorStop(0, this.numberColor);
		//grad.addColorStop(0.5, this.background);
		grad.addColorStop(1, this.background);
		this.ctx.strokeStyle = grad;
		this.ctx.lineWidth = this.radius*0.1;
		this.ctx.stroke();
		this.ctx.beginPath();
		this.ctx.arc(0, 0, this.radius*0.1, 0, 2*Math.PI);
		this.ctx.fillStyle = this.numberColor;
		this.ctx.fill();
	},

	drawNumbers: function() {
		var ang;
		var num;
		this.ctx.font = this.radius*0.18 + "px arial";
		this.ctx.textBaseline="middle";
		this.ctx.textAlign="center";
		this.ctx.fillStyle=this.numberColor;
		for(num = 1; num < 13; num++){
			ang = num * Math.PI / 6;
			this.ctx.rotate(ang);
			this.ctx.translate(0, -this.radius*0.85);
			this.ctx.rotate(-ang);
			this.ctx.fillText(num.toString(), 0, 0);
			this.ctx.rotate(ang);
			this.ctx.translate(0, this.radius*0.85);
			this.ctx.rotate(-ang);
		}
	},

	drawTime: function(ctx, radius){
		var now = new Date();
		var hour = now.getHours();
		var minute = now.getMinutes();
		var second = now.getSeconds();
		hour=hour%12;
		hour=(hour*Math.PI/6)+
		(minute*Math.PI/(6*60))+
		(second*Math.PI/(360*60));
		this.drawHand(hour, this.radius*0.5, this.radius*0.07);
		minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
		this.drawHand(minute, this.radius*0.8, this.radius*0.07);
		second=(second*Math.PI/30);
		this.drawHand(second, this.radius*0.9, this.radius*0.02);
	},

	drawHand: function(pos, length, width) {
		this.ctx.beginPath();
		this.ctx.lineWidth = width;
		this.ctx.lineCap = "round";
		this.ctx.moveTo(0,0);
		this.ctx.rotate(pos);
		this.ctx.lineTo(0, -length);
		this.ctx.stroke();
		this.ctx.rotate(-pos);
	}
};

