$(function() {

	Pace.on('hide', function(){
		$('#paceHide').animate({
			opacity: 1
		}, 800, function(){
			$(this).css({pointerEvents: 'all'});
		});
	});

    if (window.session == 0 || window.session == false || window.session == undefined) {
		Session.init(0, function() {
			initApp();
		});
	} else if (window.session > 0) {
		initApp();
	}

    $('#init_session').on('click', function () { Session.init(1, initApp); });
    $('#close_session').on('click', function () { Session.close(); });
    $('#show_income').on('click', function () { Session.showIncome(); });

	$('.nav-item a').on('shown.bs.tab', function(event){
		//setTimeout(function() {
			if ($('.container-fluid > .row').height() > $(window).height()) {
				if ($('#scrollPager').length == 0) {
					$('<div id="scrollPager" class="btn-group"> \
							<button class="btn btn-danger page-up"><span class="fa fa-chevron-up"></span></button> \
							<button class="btn btn-danger page-down"><span class="fa fa-chevron-down"></span></button> \
						</div>').appendTo('body');

					$('.page-up').on('click', function() {
						var $scrollElement = $('.tab-pane.active .scroll-content');
						console.log($scrollElement.scrollTop());
						$scrollElement.animate({
							scrollTop: $scrollElement.scrollTop() - 120
						}, 10);
					});
					$('.page-down').on('click', function() {
						var $scrollElement = $('.tab-pane.active .scroll-content');
						console.log($scrollElement.scrollTop());
						$scrollElement.animate({
							scrollTop: $scrollElement.scrollTop() + 120
						}, 10);
					});
				}
			} else {
				$('#scrollPager').remove();
			}
		//}, 200);
	});

});

function initApp() {
	Bar.init();

	$('.dropdown-item:eq(0),.dropdown-item:eq(1),.dropdown-item:eq(2)').on('click', function() {
		$('.show-mainpage').hide();
	});

	$('.nav-item:first').on('click', function() {
		$('.show-mainpage').show();
	});

	/*$(window).on('resize load', function() {
		$('#Clock').remove();
		Clock.init('Content', Math.round($(window).width()*0.15), Math.round($(window).width()*0.15));
	});*/

	scrollContentResize();
}

$(window).resize(function () {
    //if (window.session != false) {
		scrollContentResize();
		Bar.placeListResize();
	//}
});

function scrollContentResize(){
	$( ".scroll-content" ).css({
		height: $(window).height()
		, overflowY: 'auto'
    });
}
