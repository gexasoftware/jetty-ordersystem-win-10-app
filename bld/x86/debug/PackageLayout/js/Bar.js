/*
 * A Bar osztály az alap osztély ehez kapcsolódnak a Place osztályok
 */
var Bar = {};

// Bar.init() fügvényhívásnál betölti az összes helyet és létrehozza a Menu objektumot
Bar.init = function() {
	this.places = typeof objPlaces != 'undefined' ? (objPlaces) : {};
	this.menu = typeof objMenu != 'undefined' ? (objMenu) : {};
	this.numActivePlace;
	this.isEditedTables = false;
	this.isSelectTables = false;
	this.objConsumptionSelector = null;

	Bar.placeSlide();

	if (typeof Menu == 'undefined'){
		window.Menu = new MenuClass(this.menu);
	}

	$.each(this.places, function(i, e){
		window.PlaceSettings[e.id] = new PlaceSettings(e);
		if (e.status == '1'){
			Bar.initPlace(e, false);
		}
	});

	$('#editTablePosition').on('click', function(){
		var self = this;
		if (!Bar.isEditedTables){
			Bar.isEditedTables = true;
			$(this).html('<span class="fa fa-close"></span> Pozíciók mentése');
			$('#placeList').slick('slickSetOption', {swipe: false});
			$('#placeList').find('.slick-arrow').hide(100);
			$('#addTable').css({display:'block'});
			window.Place[Bar.numActivePlace].editTablePosition();
		} else {
			window.Place[Bar.numActivePlace].saveTablePosition(function(){
				Bar.isEditedTables = false;
				$(self).html('<span class="fa fa-pencil"></span> Elrendezés');
				$('#placeList').slick('slickSetOption', {swipe: true});
				$('#placeList').find('.slick-arrow').show(100);
				$('#addTable').css({display: 'none'});
			});
		}
	})

	$('#addTable').on('click', function(){
		window.Place[Bar.numActivePlace].addTable();
	});

	$('#addNewPlace').on('click', function(){
		Bar.editPlace(0);
	});
}

Bar.editPlace = function(numPlaceId) {
	var self = this;
	var objPlace = (!numPlaceId ? null : Place[numPlaceId]);
	var strHtml = '\
		<div class="form-group"> \
			<label>Hely neve azonosítója</label> \
			<input type="text" class="form-control" value="'+(!objPlace?'':objPlace.placeName)+'" id="editPlaceName"> \
		</div> \
	';

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_WARNING
		, title: 'Új hely '+(!numPlaceId?'létrehozása':'szerkesztése')
		, message: strHtml
		, buttons: [{
			label: 'Mentés'
			, icon: 'fa fa-floppy-o'
			, cssClass: 'btn-primary'
			, hotkey: 13
			, action: function(dialogRef){
				savePlaceData(dialogRef);
			}
		},
		{
			label: 'Mégsem'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
		, onshown: function(){
			$('#editPlaceName').focus();
		}
	});

	function savePlaceData(dialogRef){
		var newPlaceName = $('#editPlaceName').val();
		if (newPlaceName.length < 2){
			BootstrapDialog.show({
				type: BootstrapDialog.TYPE_DANGER
				, title: 'Hiba történt'
				, message: 'A megadott azonosító túl rövíd.'
			});
		}

		Easy.GW('place->editPlaceData', {
			place_id: numPlaceId
			, place_name: newPlaceName
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success == 1){
				if (!numPlaceId){
					new PlaceSettings(JSONResponse.data.place_data[0]);
					Bar.initPlace(JSONResponse.data.place_data[0], false);
				} else {
					Place[numPlaceId].setName(newPlaceName);
					PlaceSettings[numPlaceId].setName(newPlaceName);
				}
				dialogRef.close();
			} else {
				BootstrapDialog.show({
					type: BootstrapDialog.TYPE_DANGER
					, title: 'Hiba történt'
					, message: JSONResponse.data.msg
				});
			}
		});
	}
}
Bar.initPlace = function(objPlace, isUseIndex) {
	if (!Bar.numActivePlace){
		Bar.numActivePlace = parseInt(objPlace.id);
	}
	if (typeof window.Place[objPlace.id] == 'undefined'){
		window.Place[objPlace.id] = new Place(objPlace);
		var numSlideCount = $('#placeList').find('.slick-slide').length;
		var placeOptions = JSON.parse(objPlace.options);
		var numIndex = placeOptions.position-1;

		if (!isUseIndex || numSlideCount==0 || numSlideCount <= numIndex){
			$('#placeList').slick('slickAdd', window.Place[objPlace.id].getPlaceSkeleton());
		} else {
			$('#placeList').slick('slickAdd', window.Place[objPlace.id].getPlaceSkeleton(), numIndex, true);
			$('#placeList').slick('slickGoTo', 0);
		}
		window.Place[objPlace.id].addTables(objPlace.tables);
		if ($('#placeListTitle').text() == ''){
			$('#placeListTitle').text(window.Place[objPlace.id].placeName);
		}
	}
}
Bar.destoyPlace = function(numPlaceId) {
	if (typeof window.Place[numPlaceId] != 'undefined'){
		var numPlaceIndex = window.Place[numPlaceId].$place.index();
		if (numPlaceIndex > 0) {
			$('#placeList').slick('slickRemove', numPlaceIndex);
			delete Bar.places[numPlaceIndex];
			window.Place[numPlaceId].destroy();
		}
		$('#placeList').slick('slickGoTo', 0);
	}
}

Bar.placeSlide = function() {
	Bar.placeListResize();

	$('#placeList').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true
	});
	$('#placeList').prepend('<div id="placeListTitle"/>');

	$('#placeList').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		if (Bar.isEditedTables == true) return false;
		var numPlaceId = $(slick.$slides[nextSlide]).data('place-id');
		if (typeof window.Place[numPlaceId] != 'undefined'){
			$('#placeListTitle ').text(window.Place[numPlaceId].placeName);
			Bar.numActivePlace = parseInt(window.Place[numPlaceId].placeId);
		}
	});
}

Bar.placeListResize = function(){
	$('#placeList').css({
		height: ($(window).height() - $('nav:first').height() - 16)
	});
}

Bar.consSitoverSelectTable = function(consObj){
	this.isSelectTables = true;
	this.objConsumptionSelector = consObj;

	$('body').append('<div id="tableSelectOverlay"><h4>Kérem válasszon asztalt!</h4></div>');
	$('#tableSelectOverlay').css({
		height: $('#navBar').innerHeight() + 44
	});
}

Bar.selectTables = function(objTable){
	this.isSelectTables = false;
	this.objConsumptionSelector.sitOverThere(objTable);

	$('#tableSelectOverlay').remove();
}