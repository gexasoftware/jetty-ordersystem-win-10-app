/*
 * Ezt az menu osztálya
 * @param objData - (object) a menü kategória és termék listát tartalmazza
 */
var MenuClass = function(objData){
	if (!objData || !objData.categories || !objData.items) return false;

	this.categories = {};
	this.tblItemList = {};

	this.init(objData);
}

MenuClass.prototype.init = function(objData){
	var self = this;

	$.each(objData.categories, function(ci, ce){
		ce.options = JSON.parse(ce.options);
		self.categories[ce.options.index] = new MenuCategory(ce);
	});

	$.each(objData.items, function(ii, ie){
		$.each(self.categories, function(cti, ctd) {
			if (ctd.categoryId == ie.category_id) {
				self.categories[cti].addItem(ie);
			}
		});
	});

	$('#addMenuCategory').on('click', function(){
		self.EditMenuCategory();
	});

	$('#openCloseCategories').on('click', function() {
		if (!$(this).hasClass('closed')) {
			$('.menu-category').animate({
				height: 41
			}, 400);
			$(this).addClass('closed');
			$(this).html('<span class="fa fa-chevron-down"></span> Összes kinyitása')
		} else {
			$('.menu-category').animate({
				height: 'auto'
			}, 400);
			$(this).removeClass('closed');
			$(this).html('<span class="fa fa-chevron-up"></span> Összes becsukása')
		}
	});

	$('#menuEditList').find('>.menu-category').each(function() {
		$(this).find('>.menu-category-head').prepend('<span class="fa fa-reorder fa-sort float-left font32 white"></span>');
	});

	$('#menuEditList').sortable({
		items: '>.menu-category',
		handle: '.fa-reorder',
		stop: function() {
			var order = [];
			$('#menuEditList').find('.menu-category').each(function(i,o) {
				order.push($(this).data('id'));
			});
			Easy.GW('menu->saveCategoryOrder', {
				category_order: order
			}).done(function(JSONResponse) {
				var result = {};
				$.each(self.categories, function(index, item) {
					var n = order.indexOf(parseInt(item.categoryId))+1;
					item.categoryOptions.index = n;
					result[n] = item;
				});
				self.categories = result;
			});
		}
	});

}

MenuClass.prototype.DeleteMenuCategory = function(categoryId){
	var self = this;
	if (!categoryId) return false;

	var confirmeMessage = 'Biztos benne hogy törli a menü kategóriát?';

	if (Object.keys(this.categories[categoryId].items).length > 0)
		confirmeMessage += '<br>A törléssel a kategóriába felvett termékek is törlődnek.';

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_WARNING
		, title: 'Kategória törlése'
		, message: confirmeMessage
		, buttons: [{
			label: 'Törlöm'
			, icon: 'fa fa-trash'
			, cssClass: 'btn-warning'
			, hotkey: 13
			, action: function(dialogRef){
				DeleteCategory();
				dialogRef.close();
			}
		},{
			label: 'Mégsem'
			, icon: 'fa fa-remove'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
	});

	function DeleteCategory(){
		Easy.GW('menu->deleteCategory', {
			category_id: categoryId
		}).done(function(JSONResponse) {
			if (JSONResponse.data != null){
				if (JSONResponse.data.success != 1){
					BootstrapDialog.danger('A menü kategória törlése meghiúsult.');
				} else {
					self.categories[categoryId].delete();
				}
			}
		});
	}
}

MenuClass.prototype.EditMenuCategory = function(categoryId){
	var self = this;
	if (!categoryId) categoryId = 0;
	var strCatName = categoryId != 0 ? this.categories[categoryId].categoryName : '';
	var strCatColor = categoryId != 0 ? this.categories[categoryId].categoryOptions.color : '#111111';

	var strHtml = '';
	strHtml += '<form id="menuCategory" onsubmit="return false;" enctype="multipart/form-data">';
	strHtml += '	<input type="hidden" value="'+categoryId+'" name="category-id">';
	strHtml += '	<div class="form-group">';
	strHtml += '		<label>Kategória neve</label>';
	strHtml += '		<input type="text" class="form-control" value="'+strCatName+'" name="category-name">';
	strHtml += '	</div>';
	strHtml += '	<div class="form-group">';
	strHtml += '		<label>Kategória színe</label>';
	strHtml += '		<div class="input-group">';
	strHtml += '			<div class="input-group-addon" style="background: '+strCatColor+'">';
	strHtml += '				<span class="input-group-text" id="color-picker-addon">&nbsp;&nbsp;&nbsp;</span>';
	strHtml += '			</div>';
	strHtml += '			<input type="text" class="form-control" value="'+strCatColor+'" name="category-color" placeholder="#111111" aria-label="#111111" aria-describedby="color-picker-addon">';
	strHtml += '		</div>';
	strHtml += '	</div>';
	strHtml += '</form>';

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_INFO
		, title: 'Kategória '+(categoryId != '0'?'szerkesztése':'létrehozása')
		, message: strHtml
		, buttons: [{
			label: 'Mentés'
			, icon: 'fa fa-floppy-o'
			, cssClass: 'btn-primary'
			, hotkey: 13
			, action: function(dialogRef){
				SaveMenuCategory(dialogRef);
			}
		},{
			label: 'Mégsem'
			, icon: 'fa fa-remove'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
		, onshown: initOnShown
	});

	function initOnShown(){
		var picker = new CP(document.querySelector('input[name="category-color"]'));

		picker.on("drag", function(color) {
			this.target.value = '#' + color;
			$(this.target).prev('.input-group-addon').css({
				background: '#' + color
			});
		});
	}

	function SaveMenuCategory(dialogRef){
		var categoryData = {
			category_id: $('#menuCategory input[name="category-id"]').val(),
			category_name: $('#menuCategory input[name="category-name"]').val(),
			category_color: $('#menuCategory input[name="category-color"]').val(),
		}
		if (categoryData.category_name.length < 2){
			BootstrapDialog.danger('A megadott kategória név túl rövíd!<br>Kérem ellenőrízze!');
			return false;
		}

		var newIndex = Math.round(Object.keys(self.categories).length + 1)

		Easy.GW('menu->saveCategory', {
			category_data: categoryData
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success != 1){
				BootstrapDialog.danger('A menü kategória mentése meghiúsult.');
			} else {
				if (categoryData.category_id != 0){
					self.categories[JSONResponse.data.category_id].refreshData(categoryData)
				} else {
					var newCat = {
						id: JSONResponse.data.category_id
						, name: categoryData.category_name
						, options: "{\"index\": "+newIndex+", \"color\": \""+categoryData.category_color+"\"}"
					};
					self.categories[JSONResponse.data.category_id] = new MenuCategory(newCat);
				}
			}
			dialogRef.close();
		});
	}
}