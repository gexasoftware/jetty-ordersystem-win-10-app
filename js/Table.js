 /*
 * Ezt az osztály az egyes asztaloké
 */
var Table = function(obj){
	if (!obj || !obj.id) return;

	this.placeId = parseInt(obj.place_id);
	this.tableId = parseInt(obj.id);
	this.tableName = obj.name;
	this.options = (typeof obj.options != 'object')?JSON.parse(obj.options):obj.options;

	this.$table;
	this.$tableEdit = $('<div class="edit-table"><span class="fa fa-pencil"></span></div>');
	this.$tableDelete = $('<div class="delete-table"><span class="fa fa-trash"></span></div>');
	this.$tableName = $('<div class="table-name"></div>');
	this.$tableGuestName = $('<div class="table-guest-name"></div>');
	this.tblReservations = [];
	this.isEdit = false;

	this.tblConsumptionData = null;
	this.tblConsumptionList = {};


	if (obj.consuption.head){
		this.tblConsumptionData = obj.consuption.head;
		if (Object.keys(obj.consuption.list).length > 0)
			this.tblConsumptionList = obj.consuption.list;
	}


	this.objConsumptionEditor;

	this.init();
}
Table.prototype.init = function(){
	this.drawTable();
	this.eventListener();
}

Table.prototype.drawTable = function(){
	var self = this;
	this.$table = $('<div id="table_'+this.tableId+'" class="table-element"/>');
	window.Place[this.placeId].$place.append(this.$table);
	this.$table.append(this.$tableDelete, this.$tableEdit, this.$tableName, this.$tableGuestName);

	this.$table.css({
		position: 'absolute',
		background: 'black',
		width: 75,
		height: 75,
		top: parseInt(self.options.position.y),
		left: parseInt(self.options.position.x),
		cursor: 'pointer'
	});

	this.refreshDraw();
}

Table.prototype.refreshDraw = function(){
	if (this.tblConsumptionData != null)
		this.$tableGuestName.text(this.tblConsumptionData.guest_name);
	else
		this.$tableGuestName.text('');

	if (Object.keys(this.tblConsumptionList).length > 0){
		this.$table.addClass('has-consumption');
	} else {
		this.$table.removeClass('has-consumption');
	}

	this.$tableName.text(this.tableName);
}

Table.prototype.eventListener = function(){
	var self = this;
	this.$table.on('click', function(){
		if (!Bar.isSelectTables){
			if (!self.isEdit && Bar.numActivePlace == self.placeId){
				self.openConsumption();
			}
		} else {
			Bar.selectTables(self);
		}
	});

	this.$tableEdit.on('click', function(){
		if (self.isEdit){
			self.editTableData();
		}
	});

	this.$tableDelete.on('click', function(){
		if (self.isEdit){
			self.deleteTable();
		}
	});

}

Table.prototype.enDraggable = function(objContainment){
	this.isEdit = true;
	this.$table
		.draggable({
			cursor: "crosshair"
			, containment: objContainment
			, grid: [ 10, 10 ]
		})
		.css({
			cursor: 'move'
			, border: '2px dashed #999'
		});

	if (!this.tblConsumptionData){
		this.$tableDelete.show();
		this.$tableEdit.show();
	}
}

Table.prototype.disDraggable = function(){
	this.isEdit = false;
	this.$table
		.draggable('destroy')
		.css({
			cursor: 'pointer'
			, border: 'none'
		});

	this.$tableDelete.hide();
	this.$tableEdit.hide();
}

Table.prototype.deleteTable = function(){
	var self = this;
	if (Object.keys(this.tblConsumptionList).length > 0){
		BootstrapDialog.show({
			type: BootstrapDialog.TYPE_WARNING
			, message: 'Az asztal nem törölhető, mert van fogyasztés hozzárendelve!'
			, title: 'Asztal nem törölhető'
		});
		return false;
	} else {
		BootstrapDialog.show({
			type: BootstrapDialog.TYPE_WARNING
			, title: 'Asztal törlése'
			, message: 'Biztos benne, hogy törli az aztalt?'
			, buttons: [{
				label: 'Törlés'
				, icon: 'fa fa-trash'
				, cssClass: 'btn-primary'
				, hotkey: 13
				, action: function(dialogRef){
					tableDelete();
					dialogRef.close();
				}
			},
			{
				label: 'Mégsem'
				, hotkey: 27
				, action: function(dialogRef){
					dialogRef.close();
				}
			}]
			, modal: true
			, closable: false
		});
	}

	function tableDelete(){
		Easy.GW('place->deleteTable', {
			table_id: self.tableId
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success == 1){
				self.$table.off('click');
				self.$tableEdit.off('click');
				self.$tableDelete.off('click');

				self.$table.remove();
				delete window.Place[self.placeId].tables[self.tableId];
			}
		});
	}
}

Table.prototype.editTableData = function(){
	var self = this;
	var strHtml = '\
		<div class="form-group"> \
			<label>Asztal azonosítója</label> \
			<input type="text" class="form-control" value="'+(this.tableName!=undefined ? this.tableName : '')+'" id="editTableName"> \
		</div> \
	';

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_WARNING
		, title: 'Asztal Szerkesztése'
		, message: strHtml
		, buttons: [{
			label: 'Mentés'
			, icon: 'fa fa-floppy-o'
			, cssClass: 'btn-primary'
			, hotkey: 13
			, action: function(dialogRef){
				saveTableData(dialogRef);
			}
		},
		{
			label: 'Mégsem'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
		, onshown: function(){
			$('#editTableName').focus();
		}
	});

	function saveTableData(dialogRef){
		var newTabelName = $('#editTableName').val();
		if (newTabelName.length < 1){
			BootstrapDialog.show({
				type: BootstrapDialog.TYPE_DANGER
				, title: 'Hiba történt'
				, message: 'A megadott azonosító túl rövíd.'
			});
		}

		Easy.GW('place->editTableData', {
			table_id: self.tableId
			, table_name: newTabelName
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success == 1){
				self.tableName = newTabelName;
				self.refreshDraw();
				dialogRef.close();
			} else {
				BootstrapDialog.show({
					type: BootstrapDialog.TYPE_DANGER
					, title: 'Hiba történt'
					, message: JSONResponse.data.msg
				});
			}
		});
	}
}

Table.prototype.editGuest = function(){
	var self = this;
	var openConsumption = false;
	var strButtonText = 'Vendég módosítása';
	if (self.tblConsumptionData == null){
		var openConsumption = true;
		strButtonText = 'Vendég felvétele';

		saveGuest();
		return;
	}

	var strSkeleton = getEditGurstSkeleton(openConsumption);

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_INFO
		, title: strButtonText
		, message: strSkeleton
		, buttons: [{
			label: strButtonText
			, icon: 'fa fa-check'
			, hotkey: 13
			, action: function(dialogRef){
				saveGuest();
				dialogRef.close();
			}
		}, {
			label: 'Mégse'
			, icon: 'fa fa-remove'
			, hotkey: 27
			, action: function(dialogRef){
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
		, onshown: function(){
			$('#inputGuestName').focus();
		}
	});

	function getEditGurstSkeleton(isHasGuest){
		var strHtml = '';
		var strValue = '';
		if (isHasGuest){
			strHtml += '<p>Az asztalnál jelenleg nincs vendég!<br>Ha szeretne vendéget rendelni az asztalhoz kérem adjon meg egy nevet.</p>';
		} else {
			strHtml += '<p>Vendég neve:</p>';
			strValue = self.tblConsumptionData.guest_name;
		}
		strHtml += '<div class="form-group"><input type="text" class="form-control" id="inputGuestName" value="'+strValue+'" placeholder="Vendég neve"/></div>';
		return strHtml;
	}

	function saveGuest(){
		var strGuestName = $('#inputGuestName').val();

		Easy.GW('consumption->saveGuestToTable', {
			table_id: self.tableId
			, guest_name: strGuestName
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success == 1){
				self.tblConsumptionData = JSONResponse.data.consumption;
				self.refreshDraw();
				if (openConsumption){
					self.openConsumption();
				} else {
					self.objConsumptionEditor.refreshDraw();
				}
			} else {
				BootstrapDialog.show({
					type: BootstrapDialog.TYPE_DANGER
					, title: 'Hiba történt'
					, message: JSONResponse.data.msg
				});
			}
		});
	}
}

Table.prototype.openConsumption = function(){
	if (this.tblConsumptionData == null){
		this.editGuest();
		return false;
	}

	if (!this.objConsumptionEditor){
		this.objConsumptionEditor = new Consumption(this.placeId, this.tableId);
	} else {
		this.objConsumptionEditor.init();
	}

}

Table.prototype.deleteConsumption = function(){
	this.tblConsumptionData = null;
	this.tblConsumptionList = {};
	delete this.objConsumptionEditor;
	this.refreshDraw();
}
