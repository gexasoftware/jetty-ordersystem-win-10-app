/*
 * Ezt az osztály az egyes helyszineké
 */
var Place = function(obj){
	if (!obj || !obj.id) return false;

	this.placeId = parseInt(obj.id);
	this.placeName = obj.name;
	this.options = JSON.parse(obj.options);

	this.$place;
	this.tables = {};

	this.init();
}

Place.prototype.init = function(tblTables){
	var self = this;
	$(window).resize(function(){ self.setPlaceSize(); });
}

Place.prototype.addTables = function(tblTables){
	var self = this;
	$.each(tblTables, function(i, e){
		self.tables[e.id] = new Table(e);
	});
}

Place.prototype.getPlaceSkeleton = function(){
	this.$place = $('<div class="place-content" data-place-id="'+this.placeId+'" id="desktops_'+this.placeId+'"></div>');
	this.setPlaceSize();
	return this.$place;
}

Place.prototype.setPlaceSize = function(){
	this.$place.css({
		maxWidth: $(window).width()
		, height: ($('#placeList').height())
	});
}

Place.prototype.editTablePosition = function(){
	var self = this;
	$.each(this.tables, function(i, e){
		e.enDraggable(self.$place);
	});
}

Place.prototype.setName = function(strNewName){
	this.placeName = strNewName;
	if (Bar.numActivePlace == this.placeId){
		$('#placeListTitle ').text(strNewName);
	}
}

Place.prototype.saveTablePosition = function(callback){
	var tblTablePositions = {};
	$.each(this.tables, function(i, e){
		tblTablePositions[e.tableId] = e.$table.position();
		e.disDraggable();
	});

	Easy.GW('place->savePalceTablesPosition', {
		table_positions: tblTablePositions
	}).done(function(JSONResponse) {
		if (JSONResponse.data.success != 1){
			BootstrapDialog.show({
				type: BootstrapDialog.TYPE_DANGER
				, message: 'Nem sikerült menteni az asztalok pozícióját.'
				, title: 'Hiba történt'
			});
			return false;
		}
		if (typeof callback == 'function'){
			callback();
		}
	});

}

Place.prototype.addTable = function(){
	var self = this;
	var newTableOptions = {"position": {"x": 10, "y": 10}};

	Easy.GW('place->addTableToPlace', {
		place_id: self.placeId
		, table_options: newTableOptions
	}).done(function(JSONResponse) {
		if (JSONResponse.data.success == 1){
			if (JSONResponse.data.new_id != 0){
				var newId = JSONResponse.data.new_id;
				var newTableObj = {
					id: newId
					, options: newTableOptions
					, place_id: self.placeId
					, consuption: {
						head: false
						, list: {}
					}
				};
				var newTableObject = new Table(newTableObj);
				self.tables[newId] = newTableObject;
				newTableObject.enDraggable(self.$place);
			} else {
				BootstrapDialog.show({
					type: BootstrapDialog.TYPE_DANGER
					, message: 'Nem sikerült létrehozni az asztalt. Kérem frissítsen és próbálja meg újra.'
					, title: 'Hiba történt'
				});
			}
		}
	});

}

Place.prototype.destroy = function(){
	//this.$place.remove();
	delete window.Place[this.placeId];
	delete this;
}