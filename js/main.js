﻿(function () {
    "use strict";

    var ViewManagement = Windows.UI.ViewManagement;
    var ApplicationViewWindowingMode = ViewManagement.ApplicationViewWindowingMode;
    var ApplicationView = ViewManagement.ApplicationView;

    ViewManagement.ApplicationViewWindowingMode = 4;
    ApplicationView.preferredLaunchWindowingMode = ViewManagement.ApplicationViewWindowingMode.maximized;
    
})();


window.onload = function () {

    getFileContentAsync('host.cfg').then(function (hostname) {
        // LOAD SETTINGS

        var isHTTPS = hostname.match(new RegExp(/https:\/\//gi)) ? true : false;
        var hostData = hostname.replace('https://', '').replace('http://', '').split('@'),
            AUTH = hostData[0],
            hostUrl = (!isHTTPS ? 'http://' : 'https://') + hostData[1];
        
        Easy.GW_Options.type = 'get';
        Easy.GW_Options.url = hostUrl + '_gw_.php';
        Easy.GW_Options.headers = {
            "Authorization": "Basic " + btoa(AUTH)
        };

        Easy.GW('default->get_settings', {
            a: Math.random()
        }).done(function (JSONResponse) {

            window.session = JSONResponse.data.session;
            window.sessionDate = JSONResponse.data.sessionDate;
            window.item_add_on_click = JSONResponse.data.item_add_on_click;
            window.pagers = JSONResponse.data.pagers;
            window.objPlaces = JSONResponse.data.tblPlaces;
            window.objMenu = JSONResponse.data.tblMenu;

            initApp();
        });
    });
}

function getFileContentAsync(fileName) {
    var fileName = new Windows.Foundation.Uri("ms-appx:///" + fileName);
    return Windows.Storage.StorageFile.getFileFromApplicationUriAsync(fileName).then(function (file) {
        return Windows.Storage.FileIO.readTextAsync(file);
    });
};