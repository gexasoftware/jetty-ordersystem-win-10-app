/*
 * Ezt az menu kategória osztálya
 */
var MenuCategory = function(objData){
	if (!objData || !objData.id) return false;

	this.menuContainer = $('#menuEditList');

	this.categoryId = objData.id;
	this.categoryName = objData.name;
	this.categoryOptions = objData.options; //JSON.parse(objData.options);

	this.items = {};

	this.init();
	this.categoryListener();
}

MenuCategory.prototype.init = function(){
	var self = this;
	this.$category = $('<li class="menu-category" data-id="'+this.categoryId+'" data-index="'+this.categoryOptions.index+'"><div class="menu-category-head"><div class="menu-category-options pull-right"></div></div></li>');
	this.$categoryName = $('<h4 class="pull-left">'+this.categoryName+'</h4>');
	this.$categoryColor = $('<span class="pull-left color-tile" style="background: '+this.categoryOptions.color+'"></span>');
	this.$categoryNewItem = $('<button class="btn btn-sm pull-left"><span class="fa fa-plus"></span> Új termék hozzáadása</button>');
	this.$categoryOptions = $('<button class="btn btn-sm pull-left"><span class="fa fa-cog"></span> Szerkesztés</button>');
	this.$categoryDelete = $('<button class="btn btn-sm pull-left"><span class="fa fa-trash"></span> Törlés</button>');
	this.$categoryList = $('<ul class="list-unstyled menu-item-list" />');

	this.menuContainer.append(this.$category);

	this.$category.find('.menu-category-head').prepend(this.$categoryColor, this.$categoryName);
	this.$category.find('.menu-category-options').append(this.$categoryNewItem, this.$categoryOptions, this.$categoryDelete);
	this.$category.append(this.$categoryList);

	this.$categoryList.sortable({
		items: '>.menu-item',
		handle: '.fa-reorder',
		stop: function() {

			var order = [];
			$(this).find('.menu-item').each(function(i,o) {
				order.push($(this).data('id'));
			});

			Easy.GW('menu->saveCategoryItemOrder', {
				category_id: self.categoryId,
				item_order: order
			}).done(function(JSONResponse) {
				var result = {};
				$.each(self.items, function(index, item) {
					var n = order.indexOf(parseInt(item.itemId))+1;
					item.itemOptions.index = n;
					result[n] = item;
				});
				self.items = result;
			});
		}
	});
}

MenuCategory.prototype.categoryListener = function(){
	var self = this;

	this.$categoryNewItem.on('click', function(){
		self.EditMenuItem();
	});

	this.$categoryOptions.on('click', function(){
		Menu.EditMenuCategory(self.categoryId);
	});

	this.$categoryDelete.on('click', function(){
		Menu.DeleteMenuCategory(self.categoryId);
	});
}

MenuCategory.prototype.addItem = function(objItem){
	if (!objItem || !objItem.id) return false;

	var itemOptions = JSON.parse(objItem.options);
	this.items[itemOptions.index] = new MenuItem(objItem, this);

	this.items[itemOptions.index].$item.prepend('<span class="fa fa-reorder fa-sort float-left font24"></span>');
	this.$categoryList.sortable('refresh')
}

MenuCategory.prototype.refreshData = function(tblNewData){
	this.categoryName = tblNewData.category_name;
	this.$categoryName.text(tblNewData.category_name);

	this.categoryOptions.color = tblNewData.category_color;
	this.$categoryColor.css({background: tblNewData.category_color});
}

MenuCategory.prototype.delete = function(tblNewData){
	this.$categoryNewItem.off('click');
	this.$categoryOptions.off('click');
	this.$categoryDelete.off('click');

	this.$category.remove();
	delete Menu.categories[this.categoryId];
}

MenuCategory.prototype.EditMenuItem = function(numItemId){
	var self = this;
	if (!numItemId) numItemId = 0;
	var strItemName = numItemId != 0 ? this.items[numItemId].itemName : '';
	var objItemPrice = numItemId != 0 ? this.items[numItemId].itemPrice : {};
	var strItemMoreType = numItemId != 0 && this.items[numItemId].itemMoreType ? this.items[numItemId].itemMoreType : '0';

	var strHtml = GetEditMenuItemSkeleton(numItemId);

	EditMenuItemListener();

	BootstrapDialog.show({
		type: BootstrapDialog.TYPE_INFO
		, title: 'Termék '+(numItemId != '0'?'szerkesztése':'létrehozása')
		, message: strHtml
		, buttons: [{
			label: 'Mentés'
			, icon: 'fa fa-floppy-o'
			, cssClass: 'btn-primary'
			, hotkey: 13
			, action: function(dialogRef){
				SaveMenuItem(dialogRef);
			}
		},{
			label: 'Mégsem'
			, icon: 'fa fa-remove'
			, hotkey: 27
			, action: function(dialogRef){
				$(document).off('click', '#addMoreUnit');
				dialogRef.close();
			}
		}]
		, modal: true
		, closable: false
		, onshown: initOnShown
	});

	function initOnShown(){
		$('#itemUnitList').sortable();

		$('#editItemId').val(numItemId);
		$('#editCategoryId').val(self.categoryId);
		$('#editItemName').val(strItemName).focus();

		$('#MoreTypeTab').tab();
		if (strItemMoreType != 1){
			$('#noMoreTypeTab').tab('show')
			if (typeof objItemPrice.price != 'undefined'){
				$('#EditItemPrice').val(objItemPrice.price);
			}
			if (typeof objItemPrice.unit_piece != 'undefined'){
				$('#EditItemUnitPiece').val(objItemPrice.unit_piece);
			}
			if (typeof objItemPrice.price_units != 'undefined'){
				$('#EditItemPieceUnits').val(objItemPrice.price_units);
			}
		} else {
			$('#isMoreTypeTab').tab('show')

			$.each(objItemPrice, function(i,e){
				$('#itemUnitList').append(addListElemSKeleton(e));
			});
		}
	}

	function GetEditMenuItemSkeleton(dialogRef){
		var strHtml = '\
		<form id="menuItem" onsubmit="return false;" enctype="multipart/form-data"> \
			<input type="hidden" value="" id="editItemId"> \
			<input type="hidden" value="" id="editCategoryId"> \
			<div class="form-group"> \
				<label>Termék neve</label> \
				<input type="text" class="form-control" value="" id="editItemName"> \
			</div> \
			<div class="form-group"> \
				<label>Termék árának típusa</label> \
				<ul class="nav nav-tabs nav-fill" id="MoreTypeTab" role="tablist"> \
					<li class="nav-item"> \
						<a class="nav-link active" id="noMoreTypeTab" data-toggle="tab" href="#noMoreType" role="tab" aria-controls="noMoreType" aria-selected="true">Egyopciós</a> \
					</li> \
					<li class="nav-item"> \
						<a class="nav-link" id="isMoreTypeTab" data-toggle="tab" href="#isMoreType" role="tab" aria-controls="isMoreType" aria-selected="false">Többopciós</a> \
					</li> \
				</ul> \
			</div> \
			<div class="tab-content"> \
				<div class="col tab-pane active" id="noMoreType" role="tabpanel" aria-labelledby="noMoreTypeTab"> \
					<div class="form-group"> \
						<label>Termék ára</label> \
						<input type="text" class="form-control" value="" id="EditItemPrice"> \
					</div> \
					<div class="form-group"> \
						<label>Mennyiség / Mennyiségi egység</label> \
						<div class="input-group mb-2"> \
							<input type="text" class="form-control" value="" id="EditItemUnitPiece"> \
							<select class="form-control" id="EditItemPieceUnits"> \
								<option value="">Nincs</option> \
								<option value="adag">adag</option> \
								<option value="cl">cl (centiliter)</option> \
								<option value="dl">dl (deciliter)</option> \
								<option value="l">l (liter)</option> \
								<option value="db">db (darab)</option> \
								<option value="cs">csomag</option> \
								<option value="szel, a">szel, a</option> \
							</select> \
						</div> \
					</div> \
				</div> \
				<div class="col tab-pane" id="isMoreType" role="tabpanel" aria-labelledby="isMoreTypeTab"> \
					<div class="form-group"> \
						<label>Termék ára</label> \
						<input type="text" class="form-control" value="" id="addEditItemPrice"> \
					</div> \
					<div class="form-group"> \
						<label>Mennyiség / Mennyiségi egység</label> \
						<div class="input-group mb-2"> \
							<input type="text" class="form-control" value="" id="addEditItemUnitPiece"> \
							<select class="form-control" id="addEditItemPieceUnits"> \
								<option value="">Nincs</option> \
								<option value="adag">adag</option> \
								<option value="cl">cl (centiliter)</option> \
								<option value="dl">dl (deciliter)</option> \
								<option value="l">l (liter)</option> \
								<option value="db">db (darab)</option> \
								<option value="cs">csomag</option> \
								<option value="szel, a">szel, a</option> \
							</select> \
						</div> \
					</div> \
					<div class="form-group"> \
						<button type="button" class="btn btn-success w-100" id="addMoreUnit"><span class="fa fa-plus"></span> Hozzáad</button> \
					</div> \
					<ul id="itemUnitList" class="list-group"></ul> \
				</div> \
			</div> \
		</form>';

		return strHtml;
	}

	function EditMenuItemListener(){
		$(document).on("click", '#addMoreUnit', function(){
			var newPrice = {};
				newPrice.price = $('#addEditItemPrice').val();
				newPrice.unit_piece = $('#addEditItemUnitPiece').val();
				newPrice.price_units = $('#addEditItemPieceUnits').val();

			var strError = '';
			if (newPrice.price == '') strError += 'Kérem adja meg a árat!<br>';
			if (newPrice.unit_piece == '') strError += 'Kérem adja meg a mennyiséget!<br>';
			if (newPrice.price_units == '') strError += 'Kérem adja meg a mennyiségi egységet!<br>';

			if (strError != ''){
				BootstrapDialog.danger(strError);
				return false;
			}

			$('#addEditItemPrice').val('');
			$('#addEditItemUnitPiece').val(1);
			$('#addEditItemPieceUnits').val('');

			$('#itemUnitList').append(addListElemSKeleton(newPrice));
		});
	}

	function addListElemSKeleton(objNevPrice){
		var strHtml = '<li class="list-group-item" data-unit-price="'+objNevPrice.price+'" data-unit-piece="'+objNevPrice.unit_piece+'" data-piece-unit="'+objNevPrice.price_units+'">';
		strHtml += objNevPrice.unit_piece + ' ' + objNevPrice.price_units + ' / ' + objNevPrice.price;
		strHtml += '<button type="button" class="pull-right btn btn-sm btn-danger" onclick="$(this).parent(\'li\').remove()"><span class="fa fa-remove"></span></button>';
		strHtml += '</li>';
		return strHtml;
	}

	function SaveMenuItem(dialogRef){

		var editItemMoreType = $('#MoreTypeTab').find('.active').index('#menuItem a');
		var itemData = {
			id: $('#editItemId').val(),
			category_id: $('#editCategoryId').val(),
			name: $('#editItemName').val(),
			more_type: editItemMoreType,
		}

		if (itemData.name.length < 2){
			BootstrapDialog.danger('A megadott temék név túl rövíd! (minimum: 2 karakter)<br>Kérem ellenőrízze!');
			return false;
		}

		if (editItemMoreType != 1){
			if ($('#EditItemPrice').val() < 1){
				BootstrapDialog.danger('A megadott termékár nem valós. Kérem ellenőrízze!');
				return false;
			}
			itemData.price = {
				price: $('#EditItemPrice').val(),
				unit_piece: $('#EditItemUnitPiece').val(),
				price_units: $('#EditItemPieceUnits').val()
			}
		} else {
			if ($('#itemUnitList li').length < 1){
				BootstrapDialog.danger('Kérem vegyen fel legalább egy árat a termékhez.');
				return false;
			}
			itemData.price = [];

			$.each($('#itemUnitList li'), function(i, e){
				var newPrice = {};
				newPrice.price = $(e).data('unit-price');
				newPrice.unit_piece = $(e).data('unit-piece');
				newPrice.price_units = $(e).data('piece-unit');
				itemData.price.push(newPrice);
			});
		}

		$(document).off('click', '#addMoreUnit');

		Easy.GW('menu->saveItem', {
			item_data: itemData
		}).done(function(JSONResponse) {
			if (JSONResponse.data.success != 1){
				BootstrapDialog.danger('A menü termék mentése meghiúsult.');
			} else {
				if (itemData.id != 0){
					self.items[JSONResponse.data.item_id].refreshData(itemData)
				} else {
					itemData.id = JSONResponse.data.item_id;
					itemData.options = {
						index: Math.round(Object.keys(self.items).length + 1)
						, picture: ''
					};
					self.items[JSONResponse.data.item_id] = new MenuItem(itemData, self);
				}
			}
			dialogRef.close();
		});
	}
}