var EasyText = {};

EasyText = {
	
	mainpage: 'Főoldal',
	system_message: 'Üzenet az oldalról',
	cookie_message: 'A weboldal használatával Ön elfogadja, hogy Cookie-kat (sütiket) tároljunk számítógépén. A sütik a weboldal megfelelő működéséhez szükségesek!',
	
	// IMPORTANT!! defines currency decimal pointer
	currency_decimals: 0,
	close: 'Bezár',
	accept: 'Rendben',
	save: 'Mentés',
	cancel: 'mégse',
	error_occured: 'Hiba történt!',
	email: 'E-mail cím',
	search_no_result: 'Nincs találat!',
	information: 'Információ',
	confirm: 'Megerősítés szükséges!',
	please_select: 'Kérlek válassz...',
	input_required: 'Hibás!',
	

	/// webshop texts
	webshop_remove_item_cart_confirm: 'Törli a terméket a kosárból?',
	webshop_cart_summary: 'Összesen:',
	webshop_checkout_are_you_sure: '<strong>Véglegesíti a megrendelést?</strong><br />Kérjük győzödjön meg arról, hogy minden adatot megfelelően töltött ki!<br />Kattintson a "Rendben" gombra a megrendeléshez vagy a "mégse" gombra a visszalépéshez!',
	webshop_checkout_check_fields: 'Nem töltött ki minden kötelező mezőt, megjelöltük pirossal a hibás mezőket!',
	webshop_go_cart: 'Tovább a kosárhoz',
	webshop_cart_url: url_base + 'kosar',
	webshop_continue_shopping: 'Vásárlás folytatása!'
	
};

/* Hungarian initialisation for the jQuery UI date picker plugin. */
/* Written by Istvan Karaszi (jquery@spam.raszi.hu). */
jQuery(function($){
	$.datepicker.regional['hu'] = {
		closeText: 'bezár',
		prevText: 'vissza',
		nextText: 'előre',
		currentText: 'ma',
		closeText: 'kész',
		monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június', 'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
		monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún', 'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
		dayNames: ['Vasárnap', 'Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
		dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
		dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
		weekHeader: 'Hét',
		dateFormat: 'yy.mm.dd.',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['hu']);
});
