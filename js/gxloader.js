var gxLoader = {};

gxLoader.options = {
	type: 'line',
	width: 300,
	height: 'auto',
	item: '<span id="gx-loader-item" class="gx-loader-item"></span>',
	itemval: '▄',
	speed: 20,
	bindAjax: true,
	loaderid: 'gx-loader',
	template: '<div id="gx-loader"></div>',
	parentIt: 'body',
	top: '40%',
	bg: 'transparent',
	colorS: '#e60',
	colorE: '#000',
	font: 'Courier New',
	fontSize: '18px',
	fadeTime: 200,
	interval: false,
	itemnum: 0,
	dir: '+',
	opacity: .80,
    // circle effect
    step: 100,
    x: 0,
    y: 0,
    d: 0,
    A: 0
    // breathin effect
};

gxLoader.setColor = function(bg, forg) {
	gxLoader.options.bg = bg;
	gxLoader.options.colorS = forg;
}

gxLoader.start = function() {


	$(this.options.template).appendTo(this.options.parentIt);

	if (gxLoader.options.type != 'circle' && gxLoader.options.type != 'breathin') {
		gxLoader.options.height = gxLoader.options.height;
	} else {
		gxLoader.options.height = gxLoader.options.width;
	}

	$('#' + this.options.loaderid).hide().css({
		position: 'fixed',
		top: gxLoader.options.top,
		width: gxLoader.options.width,
		height: gxLoader.options.height,
		background: gxLoader.options.bg,
		color: gxLoader.options.colorS,
		fontFamily: gxLoader.options.font,
		padding: 3,
		fontSize: gxLoader.options.fontSize,
		left: Math.round($(this.options.parentIt).width() / 2 - gxLoader.options.width / 2),
		opacity: gxLoader.options.opacity,
		zIndex: 2999999999
	}).stop().fadeIn(gxLoader.options.fadeTime);


	if (this.options.bindAjax) {
		$(document).ajaxComplete(function() {
			$('#' + gxLoader.options.loaderid).fadeOut(gxLoader.options.fadeTime, function() {
				$(this).remove();
				gxLoader.stop();
			});
		});
	}


	switch (gxLoader.options.type) {

		case 'circle':

		var r = gxLoader.options.height / 2;

		if (!gxLoader.options.interval) {

			gxLoader.options.interval = setInterval(function() {

				$('#' + gxLoader.options.loaderid + '-item')
				.attr('id', gxLoader.options.loaderid + '-item' + gxLoader.options.itemnum)
				.attr('class', gxLoader.options.loaderid + '-item');

				$('#' + gxLoader.options.loaderid + '-item' + gxLoader.options.itemnum).css({
					position: 'absolute',
					left: gxLoader.options.x,
					top: gxLoader.options.y
				}).html(gxLoader.options.itemval).fadeOut(gxLoader.options.speed * gxLoader.options.step, function() {
					$(this).remove();
				});

				gxLoader.options.d = Math.sin(Math.PI * (gxLoader.options.A / 180)) * r;

				gxLoader.options.x = r + gxLoader.options.d;

				(gxLoader.options.dir != '-') ?
				(!(gxLoader.options.A < 90) && !(gxLoader.options.A > 270)) ?
				gxLoader.options.y = r + Math.sqrt((r * r) - (gxLoader.options.d * gxLoader.options.d)) :
				gxLoader.options.y = r - Math.sqrt((r * r) - (gxLoader.options.d * gxLoader.options.d))
				:
				(!(gxLoader.options.A < 90) && !(gxLoader.options.A > 270)) ?
				gxLoader.options.y = r - Math.sqrt((r * r) - (gxLoader.options.d * gxLoader.options.d)) :
				gxLoader.options.y = r + Math.sqrt((r * r) - (gxLoader.options.d * gxLoader.options.d));


				$('#' + gxLoader.options.loaderid).append(gxLoader.options.item);

				gxLoader.options.itemnum++;

				(!(gxLoader.options.A == 360)) ?
				gxLoader.options.A = Math.round(gxLoader.options.A + (360 / gxLoader.options.step)) :
				gxLoader.options.A = Math.round(gxLoader.options.A - 360 + (360 / gxLoader.options.step));

			}, gxLoader.options.speed);


}

break;

case 'breathin':

if (!gxLoader.options.interval) {

	gxLoader.options.interval = setInterval(function() {

		var r = gxLoader.options.height / 2;

		$('#' + gxLoader.options.loaderid + '-item')
		.attr('id', gxLoader.options.loaderid + '-item' + gxLoader.options.itemnum)
		.attr('class', gxLoader.options.loaderid + '-item');

		$('#' + gxLoader.options.loaderid + '-item' + gxLoader.options.itemnum).css({
			position: 'absolute',
			left: gxLoader.options.x,
			top: 0
		}).html(gxLoader.options.itemval).fadeOut(gxLoader.options.speed * gxLoader.options.step / 2, function() {
			$(this).remove();
		});

		gxLoader.options.d = Math.sin(Math.PI * (gxLoader.options.A / 180)) * r;

		gxLoader.options.x = r + gxLoader.options.d;

		$('#' + gxLoader.options.loaderid).append(gxLoader.options.item);

		(!(gxLoader.options.A == 360)) ?
		gxLoader.options.A = Math.round(gxLoader.options.A + (360 / gxLoader.options.step)) :
		gxLoader.options.A = Math.round(gxLoader.options.A - 360 + (360 / gxLoader.options.step));

		gxLoader.options.itemnum++;


	}, gxLoader.options.speed);



}

break;

default:

if (!gxLoader.options.interval) {

	$('#' + gxLoader.options.loaderid).append(gxLoader.options.item);
	gxLoader.options.itemnum++;

	$('.' + gxLoader.options.loaderid + '-item').css({cssFloat: 'left', fontSize: gxLoader.options.fontSize}).html(gxLoader.options.itemval);

	gxLoader.options.interval = setInterval(function() {

		var out = '';
		for (var i = 0; i < gxLoader.options.itemnum; i++)
		{
			out += gxLoader.options.itemval;
		}

		$('.' + gxLoader.options.loaderid + '-item').text(out);

		var iWidth = Math.round(gxLoader.options.width / 10);

		if (gxLoader.options.itemnum <= iWidth && gxLoader.options.dir == '+') {
			gxLoader.options.itemnum++;
			if (gxLoader.options.itemnum == iWidth) {
				gxLoader.options.dir = '-';
			}

		}

		if (gxLoader.options.itemnum >= 0 && gxLoader.options.dir == '-') {
			gxLoader.options.itemnum--;
			if (gxLoader.options.itemnum == 0) {
				gxLoader.options.dir = '+';
			}
		}
	}, gxLoader.options.speed);
}

break;

}


}

gxLoader.stop = function() {
	clearInterval(gxLoader.options.interval);
	$('#' + this.options.loaderid).remove();
	gxLoader.options.interval = false;
	gxLoader.options.itemnum = 0;
}

$(function() {
	
	gxLoader.options.top = '30%';
	
	if (gxLoader.options.bindAjax == true) {
		$(document).ajaxStart(function() {
			gxLoader.start();
		});
	}

});